* Internet ja reititys

** Huomioita

*** Sisäverkon paketit eivät kulje reitittimen kautta, vaan kytkimen. Reititin ei pysty toimimaan palomuurina sisäverkossa

*** Paketit voivat päätyä kytkinverkossa minne tahansa, joka on oletettava segregoinnissa

*** Mac osoitteet määräävät reitityksen

*** Ip ja mac osoitteet voivat olla arbitraarisia

*** Aliverkon peite on täysin käyttöjärjestelmän sisällä, eikä kulje IP paketissa

*** Jos aliverkko on tarpeeksi iso .255 ja .0 osoitteita voi jakaa asiakkaille
