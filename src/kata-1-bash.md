# Kata 1: Bash

## Learning objectives

- Knows what is a shell.
- Understands basic Bash commands.
- Can create custom Bash scripts.
- Can do basic logic in Bash.

## System target summary

The script should create the following directory structure and write the source code into `donut.{c,h}`. After that it should compile the C files into a binary at `build/donut` and run it.

```console
src/
  include/
    donut.h  # The C header file with parameters
  donut.c    # The C source code
build/
  donut      # The compiled binary
```

The script should check if `donut.h` already exists to avoid overwriting the settings.
 
You may add debugging statements to the bash script. The output of the script may look like the following, for example.

```console
[+] Running donut
```

If at any point the script experiences an error, such as a command was not found or permissions were insufficient, it must report it back to the caller and exit. An error message may be like the following.

```console
[-] Error: could not find any donuts
```

## Debriefing

TODO bash variables, bash learning material, what are builtin functions and what are binaries

Bash is a shell, which is a software package used to interact with the kernel. Bash comes pre-installed on most Linux distributions and is usually set as the default shell for each user. The shell is the program launched when the user logs in, and it presents an interactive prompt for managing the operating system.

Bash is often used in a non-interactive fashion: in scripting. Bash scripts are sequences of bash commands used to automate operating system tasks as well as system administration.

Your task is to write a Bash script which creates a C project directory structure with a spinning delicious donut application.

```text
                     @@@@@@@$$$$
                 $@@$$$$$$#########$
              #$$$$$$###***!!!!!!!!***#
            #$$$$$####**!!=!==;==!=!!!***
           ##$$$#####*!!==;;;::~:::;;=!!**
          *########**!!=;;~---,,,,~~:;==!!*
         !*#######**!!=;:~-........,-:;=!!!!
         ***#####***!==;~-.........,~:;;=!**!
        =!***##*****!==;~,.     ...,~:;=!***!
        =!*********!!!=;:-        .-:!!*###*!;
        =!!*********!!==;:-       ~=*######*!;
        ;!!*!*******!!!!=;;:     ~!#$$$$$$##!;
         ==!!!*******!!!!!==== !*#$$@@@@$$#*!:
         :;=!!!!!!!**!!!!!!!!***#$$$@@@$$#*!;
          :;==!!!!!!!!!!*******##$$$$$$$#*!;:
           :;;====!!!!!!!******########*!!!;
            ~:;;====!=!!!!!!!********!!!!=:
             ,~::;;;=====!=!!!!!!!=!!!=;:-
               ,-~~:;;;;;===========;;~-
                  ,--~~~:::::;::::~-,.
```

## Instructions

Using Bash, complete the following steps manually at first, and then automate them using a script.

> You do not need to automate the [last step](#step-5--turn-the-steps-into-a-script), as that would require infinite compute resources, which I can't afford.

Here is some reference material to help you along your way to learning to Bash things:

- All of the information, to spend the rest of your lonely life with: <https://www.gnu.org/software/bash/manual/bash.html>
- Pure information, no time for explanations, for more experienced bashers: <https://devhints.io/bash>
- Easier to understand with a bit more explanations: <https://linuxconfig.org/bash-scripting-cheat-sheet>
- Those who want instant feedback on their scripts: <https://github.com/koalaman/shellcheck>
- Redirections, useful for this kata: <https://linuxconfig.org/introduction-to-bash-shell-redirections>
- Here-documents with redirections, again useful stuff: <https://unix.stackexchange.com/questions/547988/how-to-redirect-to-a-file-from-a-heredoc>
  > Note that there is a mistake in the answer, check the [hints](#hints) when you run into it :D have fun :3

### Step 1 — Create the directories

Since this is the first exercise in the course you should create a directory to hold this and all other katas first. Choose a directory to hold the course exercise directory, for example `Documents`, and navigate to it.

#### Navigating around the shell

When you open bash, the present working directory will by default be your home directory (`/home/username` on *NIX, `C:\Users\Username` on Windows) usually indicated by the tilde character `~`. To see what is in this folder, use `ls` which lists the directory contents.

To change your working directory, use the `cd` command: `cd Documents`.

#### Creating directories

The `mkdir` command creates new directories. Now that you have navigated to your `Documents` folder, you can create a directory for the course exercises with `mkdir self-hosted`. Of course you can choose the name of the directory yourself.

`mkdir` can also create nested directories with one command. To create the course directory and the directory for this kata inside it in one go, execute `mkdir -p self-hosted/1-bash`. The `-p` is called a flag or option. They are used to pass additional information to the program. In this case the option `-p` tells mkdir to create necessary parent directories if they don't exist already.

Navigate to the newly created bash kata directory with `cd`. Now you can continue with the actual content of the exercise: Inside the kata directory, create a folder structure as described above in the target summary using your newly acquired `mkdir` skills.

### Step 2 — Create the source code files

#### Here documents

[Here-document](https://www.gnu.org/software/bash/manual/bash.html#Here-Documents) is a way to pass text as input to programs in the shell. For an example let's use the `cat` command. `cat` is a very simple program: it prints the contents of a file into the standard output, that is the command line. We can use heredoc to pass our own input to cat and print it to the screen like this:

```shell
cat << EOF
Hello world!
Heredoc works with multiple lines.
You terminate input with a line
containing EOF and nothing else.
EOF
```

This should return the same text without the first and last lines:

```shell
Hello world!
Heredoc works with multiple lines.
You terminate input with a line
containing EOF and nothing else.
```

#### Redirection

One of the most elemental concepts of the shell is redirection. All of the text that a command produces (such as the text produced by the heredoc example above) can be redirected to a file. This is handy when we want to save the output of a program permanently to the hard drive.

To save the text of the previous example to a file called `test.txt`, we have to modify the first line of the command:

```shell
cat << EOF > test.txt
Hello world!
Heredoc works with multiple lines.
You terminate input with a line
containing EOF and nothing else.
EOF
```

The `>` redirecton character tells the shell that instead of printing the output to standard output (your terminal) it should be written to the specified file.

Now you are ready to continue with the exercise. Using what you learned about heredoc, `cat` and redirection, create the following two files:

`src/donut.c`.

```c
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "donut.h"

int main() {
  float A=0,B=0,i,j,z[1760];
  char b[1760];
  printf("\x1b[2J");
  for(;;) {
    memset(b,32,1760);
    memset(z,0,7040);
    for(j=0;j<6.28;j+=0.07)
    for(i=0;i<6.28;i+=0.02) {
      float c=sin(i),d=cos(j),e=sin(A),f=sin(j),g=cos(A),h=d+2,D=1/(c*h*e+f*g+5),l=cos(i),m=cos(B),n=sin(B),t=c*h*g-f*e;
      int x=40+30*D*(l*h*m-t*n), y=12+15*D*(l*h*n+t*m), o=x+80*y, N=8*((f*e-c*d*g)*m-c*d*e-f*g-l*d*n);
      if(22>y && y>0 && x>0 && x<80 && D>z[o]) {
        z[o]=D;;;b[o]=".,-~:;=!*#$@"[N>0?N:0];
      }
    }
    printf("\x1b[H");
    for(int k=0;k<1761;k++)
    putchar(k%80?b[k]:10);
    A+=INC_A;
    B+=INC_B;
  }
}
```

Now, check if the file `src/include/donut.h` exists, and if not, write the following code into it.

`src/include/donut.h`:

```c
#ifndef DONUT_H
#define DONUT_H

float INC_A = 0.004;
float INC_B = 0.002;

#endif
```

### Step 3 — Compile the C code into an executable binary

The most common C compiler is GCC, the C compiler from the GNU compiler collection. It can be installed with `sudo apt install gcc` on Debian-based systems.

#### Compiling with GCC

Compiling C code with GCC is easy: `gcc src/donut.c` produces an executable binary. Oh wait...

```text
src/donut.c:4:10: fatal error: donut.h: No such file or directory
    4 | #include "donut.h"
      |          ^~~~~~~~~
compilation terminated.
```

You need to point GCC to the directory containing the `donut.h` header file. GCC can't find it on its own. The [hints](#hints), a search engine or `man gcc` may help you here.

After the header file problem is solved try again. This time GCC will complain about not finding the trigonometric functions `sin` and `cos`. We need to tell GCC to include the C math library in the program while compiling.

The last step is to set the output directory. Most command line tools use the flag `-o` for this, including GCC.

### Step 4 — Execute the binary

Now that you have a working executable binary in the correct directory you can run it. In bash running a binary is done by simply calling its name, including the path to it (unless it is in a directory included in the `$PATH` environment variable, but that is outside the scope of this kata). To run our program, execute `./build/donut`. In bash `.` (a dot) stands for the current directory.

### Step 5 — Turn the steps into a script

> If you are using WSL and have VSCode installed on the host system, execute `code .` to open the current directory in VSCode.

Using the steps you took up to this point, automate the task using a Bash script. Create a file with the filename extension `.sh` and write `#!/bin/bash` on the first line. This line indicates what shell should be used to run the script. In our case we want to use bash.

A bash script is essentially just a list of commands, so while doing this exercise manually you basically solved step 5 already. To automate what you just did manually in the shell, write each command you ran into the bash script on its own line. Empty lines will be ignored.

#### Make the script executable and test it

Unix-like operating systems such as Linux use a very powerful file permission system which differs in many ways from the awkward system used by Microsoft Windows. You can read more about it [here](https://www.digitalocean.com/community/tutorials/an-introduction-to-linux-permissions).

What we need to know for this exercise is that a binary or script can be ran only if it is marked as "executable". By default files will not be marked as executable, so we have to do it ourselves. The `chmod` command line tool modifies file permissions. To make our script executable, run `chmod +x myscript.sh`. The `+x` tells `chmod` to add the execute permission to the file.

Before you test your script remove all other files and directories. To remove a directory with `rm` you need to add the `-r` option, which enable recursive deletion.

Run your script in the same way you ran the donut program in [step 4](#step-4--execute-the-binary). Do not forget to add the `./` in front of the script name.

## Returning

Return these two things to the [Google Classroom assignment](https://classroom.google.com/c/NjAzNDI4MzQ3Mjc4/a/NjAzNDMwMzY5OTM3/details):

- a screenshot of _the spinning donut_.
- the final Bash script from [step 5](#step-5--turn-the-steps-into-a-script).

Finally, please fill the [kata feedback form](https://docs.google.com/forms/d/e/1FAIpQLScuGcebcVyQNcLSoYY2m4HbxXunYRfMpB-6TLIFS7inweXgYg/viewform?usp=sf_link), and then move on to the next kata by pressing right.

## Commands and tools

| Command or tool                | Summary                        |
|--------------------------------|--------------------------------|
| [`bash`](./commands.md#bash)   | {{#include commands.md:bash}}  |
| [`cd`](./commands.md#cd)       | {{#include commands.md:cd}}    |
| [`ls`](./commands.md#ls)       | {{#include commands.md:ls}}    |
| [`mkdir`](./commands.md#mkdir) | {{#include commands.md:mkdir}} |
| [`rm`](./commands.md#rm)       | {{#include commands.md:rm}}    |
| [`cat`](./commands.md#cat)     | {{#include commands.md:cat}}   |
| [`gcc`](./commands.md#gcc)     | {{#include commands.md:gcc}}   |
| [`man`](./commands.md#man)     | {{#include commands.md:man}}   |
| [`echo`](./commands.md#echo)   | {{#include commands.md:echo}}  |
| [`chmod`](./commands.md#chmod) | {{#include commands.md:chmod}} |

## Hints

- Supply `-I` and `-l` options for GCC with appropriate arguments.
- Make sure `$@` on line 18 of `donut.c` doesn't get interpreted as a shell variable.
