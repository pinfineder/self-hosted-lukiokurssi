# Kata 3: Ansible
## Learning objectives

- Understands the advantages of using Ansible over Bash for system administration
- Can provision Linux systems with Ansible, including copying files and installing packages
- Knows how Jinja templates can be used with Ansible

## System target summary

- The Ubuntu host should be updated to the latest version.
- The Fish shell should be installed on the host.
- A new user should be created (because using the admin for everything is not a good idea).
- The new user should have the Fish shell set as the default shell.
  - The user should be greeted with the `neofetch` command when launching a new shell.

## Debriefing

Your task is to provision a remote server using Ansible. The remote server is a Ubuntu box with an internet connection.

Start by installing `ansible` on your controller. In Ansible lingo, we call the local machine the _controller_, and the target servers are called _hosts_.

## Instructions

Follow the following instructions on the controller only. I suggest creating a new directory for this kata.

### Step 1 — Ansible playbook

Create a new file named `kata3.yml` with the following contents:

```yaml
- hosts: all
  become: true
  vars:
    new_user: fablab
    shell: /usr/bin/fish
    use_neofetch: yes
    unattended_mail: firstname.lastname@eduespoo.fi

  tasks:
  - name: Update system
    apt:
      update_cache: yes
      upgrade: dist
      
  - name: Install bloat
    apt:
      name:
      - fish
      - neofetch
      state: present

  - name: Create new user
    user:
      name: "{{ new_user }}"
      groups: sudo
      createhome: yes

  - name: Create the fish config directory
    file:
      path: /home/{{ new_user }}/.config/fish
      state: directory
      owner: "{{ new_user }}"
      group: "{{ new_user }}"

  - name: Copy fish config template
    template:
      src: templates/config.fish.j2
      dest: /home/{{ new_user }}/.config/fish/config.fish
      owner: "{{ new_user }}"
      group: "{{ new_user }}"
      mode: 0644

  - name: Check if reboot required
    stat:
      path: /var/run/reboot-required
    register: reboot_required_file
  
  - name: Reboot if system was upgraded
    reboot:
    when: reboot_required_file.stat.exists == true
```

This is called an [Ansible playbook](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_intro.html).

The playbook performs the following _tasks_:

1. Update the system using the `apt` module.
1. Install some software using the `apt` module.
1. Create a new user with sudo rights using the `user` module.
1. Create the fish configuration directory for the new user using the `file` module.
1. Copy the fish configuration template to the new user's home directory using the `template` module.
1. Check if reboot is required (`stat`) and `reboot`.

### Step 2 — Host inventory

Create a `hosts.yml` file with the following contents:

```yaml
all:
  hosts:
    ubuntu:
      ansible_host: 1.3.3.7
  vars:
    ansible_user: localadmin
    ansible_ssh_common_args: -J jump@bastion.fablab.rip
```

Replace the IP address to point to your Fablab Virtual Server instead.

Here's what each line in the file means:

1. `all`: This is the top-level group in the inventory file. You can define additional groups within the all group if you need to manage multiple hosts.
1. `hosts`: This is the group that contains all the hosts defined in the inventory file.
1. `ubuntu`: This is the name of the host that we're defining in the inventory file. You can replace `ubuntu` with anything, e.g. `b1-ubuntu-2`.
1. `ansible_host`: This specifies the IP address or hostname of the Ubuntu machine that you want to manage with Ansible.
1. `ansible_user`: This specifies that Ansible should connect to the machine using the `localadmin` user. You can replace it with any valid username on the Ubuntu machine that has SSH access.
1. `ansible_ssh_common_args`: This is the SSH jump host configuration required for accessing the fablab internal network from the internet. [Read more](https://blog.keyboardinterrupt.com/ansible-jumphost/)

Try running

```console
ansible all -i hosts.yml -m ping
```

to see if your server is reachable.

> [Read more about Ad-hoc commands](https://www.digitalocean.com/community/cheatsheets/how-to-manage-multiple-servers-with-ansible-ad-hoc-commands)

### Step 3 — Provisioning

Run the ansible playbook to provision the hosts:

```console
ansible-playbook kata3.yml -i hosts.yml
```

Updating the system may take some time as the servers are limited by school internet speed. It does not report any progress in real time.

> If the `localadmin` user didn't have passwordless sudo, we could pass in `--ask-become-pass` to ask ansible to ask us for the sudo password interactively.

### Step 4 — Fish configuration

Next, create a new file at `templates/config.fish.j2` with the following contents:

```jinja
function fish_greeting
    {% if use_neofetch %}
    neofetch
    {% endif %}
end
```

These special markings `{% %}` are meta-configuration using the [Jinja templating language](https://jinja.palletsprojects.com/en/2.10.x/). It integrates seamlessly with Ansible and gets the variable `use_neofetch: yes` from the playbook file `kata3.yml`.

> `yes` and `true` are equivalent in Ansible

Inside the markings we can write python-like instructions, e.g. `if use_neofetch` will tell Jinja to include the text `neofetch` if and only if `use_neofetch` is truthy.

### Step 5 — Setting the default shell

Add a new task to the Ansible playbook to set the `fish` shell as default for the `new_user` user. What would be the best spot in the playbook to set the shell at?

Now run the playbook to see if it's doing things.

```console
ansible-playbook -v kata3.yml -i hosts.yml
```

> Use the `-v` option for increased verbosity

If you get syntax errors from Ansible, make sure your indentation is correct and you are using spaces instead of tabs. FYI lists can be indented with 0 spaces if you like. Just try to avoid mixing the styles.

```yaml
a list:
- with
- no
- indentation
```

### Step 6 — Automatic updates

Next you will set up automating automatic updates like in the [previous kata](./kata-2-linux.md#step-5--automatic-updates). But this time you will be using Ansible. 

Add these tasks before `Reboot`.

```yaml
- name: Install unattended upgrades package
  apt:
    name: unattended-upgrades
    state: present

- name: Update unattended upgrades configuration
  template:
    src: templates/50unattended-upgrades.j2
    dest: /etc/apt/apt.conf.d/50unattended-upgrades
    mode: 0644
    owner: root
    group: root

- name: Restart unattended upgrades service
  service:
    name: unattended-upgrades
    state: restarted
```

This requires the following template file, so create it in the `templates` directory: `templates/50unattended-upgrades.j2`.

```jinja2
// Unattended-Upgrade::Origins-Pattern controls which packages are
// upgraded.
//
// Lines below have the format "keyword=value,...".  A
// package will be upgraded only if the values in its metadata match
// all the supplied keywords in a line.  (In other words, omitted
// keywords are wild cards.) The keywords originate from the Release
// file, but several aliases are accepted.  The accepted keywords are:
//   a,archive,suite (eg, "stable")
//   c,component     (eg, "main", "contrib", "non-free")
//   l,label         (eg, "Debian", "Debian-Security")
//   o,origin        (eg, "Debian", "Unofficial Multimedia Packages")
//   n,codename      (eg, "jessie", "jessie-updates")
//     site          (eg, "http.debian.net")
// The available values on the system are printed by the command
// "apt-cache policy", and can be debugged by running
// "unattended-upgrades -d" and looking at the log file.
//
// Within lines unattended-upgrades allows 2 macros whose values are
// derived from /etc/debian_version:
//   ${distro_id}            Installed origin.
//   ${distro_codename}      Installed codename (eg, "buster")
Unattended-Upgrade::Origins-Pattern {
        "origin=Ubuntu,archive=${distro_codename}-security";
        "origin=Ubuntu,archive=${distro_codename}-updates";
        "origin=Ubuntu,archive=${distro_codename}-proposed";
        "origin=Ubuntu,archive=${distro_codename}-backports";
};

// Send email to this address for problems or packages upgrades
// If empty or unset then no email is sent, make sure that you
// have a working mail setup on your system. A package that provides
// 'mailx' must be installed. E.g. "user@example.com"
Unattended-Upgrade::Mail "{{ unattended_mail }}";

// Set this value to one of:
//    "always", "only-on-error" or "on-change"
// If this is not set, then any legacy MailOnlyOnError (boolean) value
// is used to chose between "only-on-error" and "on-change"
Unattended-Upgrade::MailReport "on-change";

// Remove unused automatically installed kernel-related packages
// (kernel images, kernel headers and kernel version locked tools).
Unattended-Upgrade::Remove-Unused-Kernel-Packages "true";

// Automatically reboot *WITHOUT CONFIRMATION* if
//  the file /var/run/reboot-required is found after the upgrade
Unattended-Upgrade::Automatic-Reboot "true";

// If automatic reboot is enabled and needed, reboot at the specific
// time instead of immediately
//  Default: "now"
Unattended-Upgrade::Automatic-Reboot-Time "02:00";

// Enable logging to syslog. Default is False
Unattended-Upgrade::SyslogEnable "true";
```

You don't need to understand this configuration file. Just notice the line `Unattended-Upgrade::Mail "{{ unattended_mail }}";` which will be used to send you important news.

> If you want to learn more advanced Ansible concepts, try to integrate and configure [this community _role_](https://github.com/hifis-net/ansible-role-unattended-upgrades).

Now SSH in to the machine and check the output of the following commands:

```shell
systemctl status unattended-upgrades
journalctl -eu unattended-upgrades
```

> To exit the journalctl pager view, press `Q`.

### Task 7 — Check the results

Switch user to the newly created user using `sudo su - fablab`. If everything worked correctly, you should see a vibrant welcome message.

If you want, you can change the `new_user` variable to `localadmin` to see what happens.

> Something to think about: should changing the `new_user` variable revert the previous user's shell back to what it was? How could that work?
>
> Even though Ansible is (usually) idempotent, it does not fully declare the target state of the systems only some part of it. Changing the scope of what is managed by Ansible (like the `new_user` variable) is bound to result in inconsistency and irreproducibility.
>
> Ideally every file in the system would be managed by Ansible, but what about differences in hardware? Should Ansible also buy the servers?
>
> For virtual servers this isn't a ridiculous idea. A commonly used tool to provision infrastructure is [Terraform](https://www.terraform.io/).

## Returning

Return the Ansible playbook `kata3.yml` to the Google Classroom assignment along with a screenshot of the email that you received from the unattended uprade (may take a few days) (also check trash).

Finally, please fill the [kata feedback form](https://docs.google.com/forms/d/e/1FAIpQLScuGcebcVyQNcLSoYY2m4HbxXunYRfMpB-6TLIFS7inweXgYg/viewform?usp=sf_link), and then move on to the next kata by pressing right.

## Ansible modules

List of the Ansible modules used in this Kata:

| Module                                                                                                 | Description                          |
|--------------------------------------------------------------------------------------------------------|--------------------------------------|
| [`apt`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html)           | Manages apt-packages                 |
| [`user`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html)         | Manage user accounts                 |
| [`file`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html)         | Manage files and file properties     |
| [`template`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html) | Template a file out to a target host |
| [`stat`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/stat_module.html)         | Retrieve file or file system status  |
| [`reboot`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/reboot_module.html)     | Reboot a machine                     |

## Commands and tools

| Command or tool                                            | Summary                                      |
|------------------------------------------------------------|----------------------------------------------|
| [`ssh`](./commands.md#ssh)                                 | {{#include commands.md:ssh}}                 |
| [`neofetch`](./commands.md#neofetch)                       | {{#include commands.md:neofetch}}            |
| [`fish`](./commands.md#fish)                               | {{#include commands.md:fish}}                |
| [`unattended-upgrades`](./commands.md#unattended-upgrades) | {{#include commands.md:unattended-upgrades}} |
| [`ansible`](./commands.md#neofetch)                        | {{#include commands.md:ansible}}             |
| [`ansible-playbook`](./commands.md#ansible-playbook)       |                                              |

## Hints

N/A

> Suggest some hints in the feedback form
