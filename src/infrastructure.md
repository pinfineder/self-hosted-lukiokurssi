# Fablab Server Hall Infrastructure

## FAQ

### I cannot connect to my server :C

Ask the staff to reprovision your server or to give you [tty](https://en.wikipedia.org/wiki/Tty_(Unix)) access through [SPICE](https://www.spice-space.org/).

