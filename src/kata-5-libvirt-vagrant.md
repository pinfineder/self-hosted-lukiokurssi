# Kata 5: Libvirt and Vagrant

## Learning objectives

- Can create VMs manually using Vagrant and libvirt.
- Knows the differences between and use cases of Vagrant and libvirt.

## System target summary

VM server and the VM should be in the following state:
- The VM server:
  - Should be updated to the latest version.
  - Should have libvirt and Vagrant installed.
  - Should have a Vagrant managed Ubuntu 22.04 VM running and accessible through SSH locally.
- The VM:
  - Should have 1GiB of memory.
  - Should have the project folder (`./`) available at `/vagrant`.
  - May have the user `fablab` and `fish` and `neofetch` installed as described in [kata 2](./kata-2-linux.md).

## Debriefing

TODO

### Libvirt

<https://ubuntu.com/server/docs/virtualization-libvirt>

### Vagrant

[Vagrant](https://www.vagrantup.com/) is a tool for creating and managing virtual machines. Vagrant itself is not a virtual machine hypervisor. It supports different hypervisors, like KVM or VirtualBox. Vagrant streamlines setting up custom local virtual machines for development use.

The virtual machine images, known as Vagrant boxes, can be downloaded and updated from the internet. You can search for the available boxes from [app.vagrantup.com](https://app.vagrantup.com/boxes/search).

### Terraform

TODO how fablab infrastructure is provisioned

### Infrastructure

There will be 3 machines in use during this exercise which can get quite complicated. Each machine has network interface controllers (NICs) that may correspond to physical devices such as an Ethernet controller or Wi-Fi card. We don't care what the device or the medium is, only the name of the

1. The local controller machine, e.g. a student laptop, where Ansible and an SSH client are installed.

   | NIC network | IP address  |
   |-------------|-------------|
   | LAN         | 10.0.0.x/18 |
   
1. The Fablab Virtual Server (VM server), where libvirt, ansible and Vagrant are installed. Provisioned by the first machine. This machines spawns the third machine.

   | NIC network    | IP address       |
   |----------------|------------------|
   | fablab LAN     | 10.42.0.x/24     |
   | Virtual Bridge | 192.168.122.1/24 |

1. The virtual machine. Running on the VM server under Vagrant.

   | NIC network | IP address       |
   |-------------|------------------|
   | libvirt LAN | 192.168.122.x/24 |


## Instructions

### Step 1 — Installation

On the VM server:

1. Check that KVM is available and install libvirt. On Ubuntu follow [these instructions](https://ubuntu.com/server/docs/virtualization-libvirt).
1. Install Vagrant.

### Step 2 — Vagrant project

On the VM server:

1. Create a directory, for example `kata5` for the Vagrant project, and change directory to it.
1. Initialize the Vagrant project with the box `generic/ubuntu2204`:
   ```console
   vagrant init generic/ubuntu2204
   ```
   > Note that this image is 1.6G in size and may take quite long to download. You are allowed to use any GNU/Linux distribution that has a smaller image instead.
1. Launch the VM:
   ```console
   vagrant up --provider=libvirt
   ```
   > If you get a Permission denied error, make sure you are part of the `libvirt` group and try to relogin.
1. Connect to it over SSH and check if `/vagrant` is mounted:
   ```console
   $ vagrant ssh
   vagrant@ubuntu2204:~$ ls /vagrant
   ls: cannot access '/vagrant': No such file or directory
   ...
   ```
   
   > Aternatively you can connect with plain old SSH. The IP address of the VM is mentioned in the output during vagrant up. `default: SSH address: 192.168.121.225:22`.
   >
   > To connect to the VM directly with jump hosts, try:
   > ```console
   > ssh -J jump@bastion.fablab.rip,localadmin@1.3.3.7 vagrant@192.168.121.225
   > ```
1. Exit or login to your VM server in another terminal. Check for running Vagrant VMs using `global-status`:
   ```console
   $ vagrant global-status
   id       name    provider state   directory
   ----------------------------------------------------------------------
   95c6abc  default libvirt running /home/localadmin/kata5
   ```
1. Now in the project directory, destroy the VM
   ```console
   $ vagrant destroy
   default: Are you sure you want to destroy the 'default' VM? [y/N] y
   ```
   
### Step 3 — Shared directory

1. [Configure the VM to share](https://vagrant-libvirt.github.io/vagrant-libvirt/examples.html#synced-folders) the project directory (`./`) on the host at `/vagrant` in the guest VM. The sync type should preferably be [`virtiofs`](https://virtio-fs.gitlab.io/index.html).

   > Here are the configuration options for the libvirt provider: <https://vagrant-libvirt.github.io/vagrant-libvirt/configuration.html>
1. Connect to the VM over SSH and check that the shared folder is mounted:
   ```console
   $ vagrant ssh
   vagrant@ubuntu2204:~$ mount
   ...
   9a9bf8f41aeb89c22233ecacf3fb8f7 on /vagrant type virtiofs (rw,relatime)
   vagrant@ubuntu2204:~$ ls -tlh /vagrant
   total 4.0K
   -rw-r--r-- 1 vagrant vagrant 3.2K Mar  4 17:40 Vagrantfile
   ```

## Returning and feedback

## Commands and tools

| Command or tool                              | Summary                               |
|----------------------------------------------|---------------------------------------|
| [`ssh`](./commands.md#ssh)                   | {{#include commands.md:ssh}}          |
| [`vagrant`](./commands.md#vagrant)           | {{#include commands.md:vagrant}}      |

## Hints

N/A
