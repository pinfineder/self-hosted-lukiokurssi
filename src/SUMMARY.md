# Summary

[Introduction](introduction.md)

---

- [Lecture 0: Meta](./lecture-0-meta.md)
- [Kata 1: Bash](./kata-1-bash.md)
- [Kata 2: Linux](./kata-2-linux.md)
- [Kata 3: Ansible](./kata-3-ansible.md)
- [Lecture 4: Virtual Machine Basics](./lecture-4-virtual-machine-basics.md)
- [Kata 5: Libvirt and Vagrant](./kata-5-libvirt-vagrant.md)
- [Lecture 6: Networking Basics](./lecture-6-networking-basics.md)
- [Kata 7: Networking Tools](./kata-7-networking-tools.md)
- [Kata 8: System Services]()
- [Kata 9: Internet Security]()
- [Kata 10: Secret Management]()
- [Kata 11: Virtual Private Networks]()
- [Kata 12: High-performance computing]()
- [Kata 13: NIX]()
- [Bonus Kata 1: Dotfiles]()

[Command reference](./commands.md)
[Fablab Server Hall Infrastructure](./infrastructure.md)
[Hardware](./hardware.md)
