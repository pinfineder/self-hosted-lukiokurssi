# Hardware

## Fujitsu Esprimo E920

The motherboard serial number can be found on the small sticker on the motherboard next to the powersupply. To enter boot menu press F12. By default all machines have 16GiB of RAM.

| MAC               | S/N        | Motherboard S/N | Bios status | Notes                                                               |
|-------------------|------------|-----------------|-------------|---------------------------------------------------------------------|
| 90:1B:0E:1C:0C:83 | YLQN009378 | 43387839        | LOCKED      |                                                                     |
| 90:1B:0E:1C:87:C5 | YLQN009383 | 43388353        | UNLOCKED    | 32GiB RAM                                                           |
| 90:1B:0E:1C:87:D9 | YLQN009380 | 43388382        | UNLOCKED    |                                                                     |
| 90:1B:0E:1C:88:70 | YLQN009369 | 43388016        | LOCKED      |                                                                     |
| 90:1B:0E:2E:35:16 | YLQN013881 | 44415335        | LOCKED      | 16GiB ECC RAM                                                       |
| 90:1B:0E:2E:35:24 | N/A        | 44415302        | LOCKED      | Wrong topcase                                                       |
| 90:1B:0E:2E:35:31 | YLQN013889 | 44413560        |             |                                                                     |
| 90:1B:0E:46:95:68 | YLQN018353 | 45933383        | UNLOCKED    | 16GiB ECC RAM, (?PXE boot issue (probably solved by disabling CSM)) |
| 90:1B:0E:46:95:6E | YLQN018348 | 45933377        | LOCKED      | 16GiB ECC RAM                                                       |
| 90:1B:0E:46:97:87 | N/A        | 45962404        | LOCKED      | Wrong topcase                                                       |
| 90:1B:0E:46:97:89 | YLQN018381 | 45962486        | UNLOCKED    |                                                                     |
| 90:1B:0E:46:97:6A | YLQN018378 | 45962355        | UNLOCKED    |                                                                     |
| 90:1B:0E:46:97:6C | YLQN018373 | 45962395        | LOCKED      |                                                                     |
| 90:1B:0E:46:97:6D | YLQN018374 | 45962392        | LOCKED      |                                                                     |
| 90:1B:0E:46:97:77 | YLQN018375 | 45962386        | LOCKED      |                                                                     |

## Fujitsu Esprimo E910

The motherboard serial number can be found on the small sticker on the motherboard next to the powersupply.

| MAC               | S/N        | Motherboard S/N | Bios status | Notes                  |
|-------------------|------------|-----------------|-------------|------------------------|
| 00:19:99:F1:C5:D1 | YLHM014057 | 41621181        | UNLOCKED    | PCI Ethernet installed |

## HP EliteDesk 800 G2

The serial number can be found on the case next to the rear IO panel.

| MAC               | S/N        | Bios status | Notes |
|-------------------|------------|-------------|-------|
|                   | CZC61293YJ | UNLOCKED    |       |
| 70:5A:0F:32:6A:50 | CZC648C00X | UNLOCKED    |       |

## SuperMicro X8SAX

Equibbed with 24GiB RAM. To enter BIOS press Delete key.

| MAC               | S/N | Bios status | Notes |
|-------------------|-----|-------------|-------|
| 00:30:48:D0:F3:AC |     | UNLOCKED    |       |

## Storage

| Name                               | Count | Capacity | Installed to                                                                                                                                                                                 |
|------------------------------------|-------|----------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Intenso SATA SSD                   | 3     | 128GB    | 90:1B:0E:1C:0C:83, 90:1B:0E:2E:35:31, 90:1B:0E:1C:87:D9                                                                                                                                      |
| Fanxiang SATA SSD                  | 12    | 128GB    | 90:1B:0E:46:97:87, 90:1B:0E:1C:87:C5, 90:1B:0E:46:97:77, 90:1B:0E:46:97:6D, 90:1B:0E:46:97:89, 90:1B:0E:46:95:68, 90:1B:0E:1C:88:70, 90:1B:0E:2E:35:16, 90:1B:0E:2E:35:24, 00:30:48:D0:F3:AC |
| Intenso M.2 SATA, M.2 SATA adapter | 1     | 128GB    |                                                                                                                                                                                              |
| Intenso M.2 NVMe Premium           | 1     | 250GB    |                                                                                                                                                                                              |
| Patriot M.2 NVMe                   | 1     | 128GB    | 70:5A:0F:32:6A:50                                                                                                                                                                            |
| JinyJaier unlabeled SATA SSD       | 1     | 120GB    |                                                                                                                                                                                              |

## Networking

| Name                                                                   | Count | Interfaces            | Installed to |
|------------------------------------------------------------------------|-------|-----------------------|--------------|
| TP-Link Archer AC1200                                                  | 1     | PCIe, WiFi, Bluetooth |              |
| D-Link DGE 528T                                                        | 1     | PCI, Ethernet         |              |
| ASUS 10G Network Card, Green                                           | 1     | PCIe, WiFi            | routeri      |
| CSL network card WLAN strong reception high performance double antenna | 1     | PCIe, WiFi            |              |
| SATA adapter card                                                      | 2     | PCIe, SATA            |              |

## Rack server positions

Top shelf is 1. Right column is A/C. Botmost server is 1.

| MAC               | Shelf | Position |
|-------------------|-------|----------|
| 90:1B:0E:2E:35:31 | 1     | A1       |
| 90:1B:0E:46:97:6C | 1     | A2       |
| 90:1B:0E:46:97:6A | 1     | A3       |
| 90:1B:0E:46:97:87 | 1     | A4       |
| 70:5A:0F:32:6A:50 | 1     | B1       |
| 90:1B:0E:1C:0C:83 | 1     | B2       |
| 90:1B:0E:46:97:77 | 1     | B3       |
| 90:1B:0E:46:97:6D | 1     | B4       |
| 90:1B:0E:46:97:89 | 2     | C1       |
| 90:1B:0E:46:95:68 | 2     | C2       |
| 90:1B:0E:46:95:6E | 2     | C3       |
| 90:1B:0E:1C:87:C5 | 2     | C4       |
| 90:1B:0E:1C:87:D9 | 2     | D1       |
| 90:1B:0E:1C:88:70 | 2     | D2       |
| 90:1B:0E:2E:35:16 | 2     | D3       |
| 90:1B:0E:2E:35:24 | 2     | D4       |
