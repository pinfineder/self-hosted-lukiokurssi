# Lecture 4: Virtual Machine Basics

## Learning objectives

- Understands what a virtual machine is.
- Knows common Linux virtual machine management software: QEMU, Libvirt.
- Understands the benefits of virtualization.
- Knows what containerization is.

## Introduction

In this lecture you will be introduced to many concepts in the world of virtualization. The world is full of emerging as well as legacy software and technologies, and virtualization is a total black hole of information and can be hard to get a grasp of.

Let's start with covering some terms that are used in this field.

| Term                                                                    | Meaning                                                                                                                                                         |
|-------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Virtualization](https://en.wikipedia.org/wiki/Virtualization)          | A technology that allows multiple workloads to share a set of resources, such as the CPU and RAM.                                                               |
| [Hypervisor](https://en.wikipedia.org/wiki/Hypervisor)                  | The virtual machine monitor/manager. Creates and runs the virtual machines.                                                                                     |
| [Virtual machine](https://en.wikipedia.org/wiki/Virtual_machine)        | An emulation of a physical computer system running on a hypervisor.                                                                                             |
| Host                                                                    | The server running the hypervisor hosting the virtual machines (or containers). Also used as a prefix: host operating system, host kernel.                      |
| Guest                                                                   | The virtual machine (or container) running on the host. Sometimes also called a host in other contexts (e.g. ansible).                                          |
| [Cloud computing](https://en.wikipedia.org/wiki/Cloud_computing)        | Using other people's computer systems. Most internet services are in the cloud as opposed to [on-premises](https://en.wikipedia.org/wiki/On-premises_software). |
| [Cluster](https://en.wikipedia.org/wiki/Computer_cluster)               |                                                                                                                                                                 |
| [Container](https://en.wikipedia.org/wiki/Containerization_(computing)) | Lightweight and isolated virtual operating system environment. Unlike virtual machines, containers share the kernel with the host.                              |

## Virtual Machines

Virtual machines (VMs) are software-based emulations of physical computers, which allow multiple operating systems to run on a single physical computer simultaneously. Each VM behaves like a separate computer, with its own CPU, memory, storage, and networking resources. Applications running inside a VM can't see or interfere with applications running in other VMs or on the host system.

## Virtualization

Virtualization allows multiple operating systems to share hardware simultaneously and efficiently, while emulation is based on mimicking the behavior of one system on another system. Emulation typically involves translating instructions and hardware behavior from one system to another, which can result in slower performance than virtualization. 

> Additionally, emulation is often used to run older software on newer hardware, while virtualization is used to create virtual hardware for purposes such as server development and testing.

Virtualization brings a lot of benefits to the table, like:

- Resource optimization: Virtualization allows multiple virtual machines to run on a single physical server, maximizing resource utilization and reducing the number of physical servers required.
- Scalability: Virtualization enables more logical scaling of resources up or down based on demand.
- Isolation and security: Processes and memory are isolated between virtual machines running on the same physical machine providing an additional layer of security. However, virtual machines might still be able to connect to each other over the network.
- Disaster recovery: Virtualization can simplify disaster recovery by allowing virtual machines to be easily migrated between physical servers or backed up and restored as needed.
- Simplified management: Virtualization allows for centralized management of servers, often simplifying tasks such as software updates, backups, recovery and resource allocation and load distribution.

> Most processors support accelerated virtualization, but it might require toggling an option in the BIOS. Intel processors VT-x and VT-d and on AMD it's AMD-V.

## KVM, QEMU and Libvirt

[KVM](https://en.wikipedia.org/wiki/Kernel-based_Virtual_Machine) (Kernel-based Virtual Machine) is a type of virtualization technology for Linux that allows the Linux kernel to act as a [hypervisor](https://en.wikipedia.org/wiki/Hypervisor), enabling it to host multiple VMs. KVM provides hardware virtualization, which means that VMs can run at near-native performance because they are using the host system's physical hardware directly.

[QEMU](https://en.wikipedia.org/wiki/QEMU) (Quick Emulator) is a highly flexible and portable emulator. In addition to the processor, QEMU can emulate the disk, network, VGA, PCI, and USB peripherals of the guest VM. KVM integrates with QEMU to provide native CPU acceleration by the host kernel. [Read more](https://serverfault.com/questions/208693/difference-between-kvm-and-qemu). It is even possible to run VMs inside VMs with KVM — this is known as [nested virtualization](https://www.howtogeek.com/devops/how-to-enable-nested-kvm-virtualization/).

The following image shows the interaction between different components of the operating system and QEMU.

![QEMU/KVM](https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Kernel-based_Virtual_Machine.svg/966px-Kernel-based_Virtual_Machine.svg.png)

> Another popular virtual machine hypervisor is [VirtualBox](https://en.wikipedia.org/wiki/VirtualBox), but because it lacks features and its Extension Pack is proprietary software we won't use it.

[Libvirt](https://libvirt.org/) is a Linux toolkit that unifies the interface for managing virtual machines on different virtualization technologies and hypervisors. It provides an abstraction layer for interacting with QEMU for example. Its features include

- Creating, starting, stopping virtual machines, virtual networks, virtual disks
- [Virtual networking](https://wiki.libvirt.org/VirtualNetworking.html)
- Everything is configured using [XML](https://libvirt.org/format.html) documents
- VMs are called domains
- Migrating domains 
- Attaching to domains graphically
- Can be fully managed over SSH

In the next image you can see multiple servers each running Ubuntu VMs in libvirt. The application used is [`virt-manager`](https://virt-manager.org/) and all the connections are done over SSH and the internet.

![Virt manager SSH](./virt-manager1.png)

Some more pictures of remote management for your enjoyment

<p align="center">
<img alt="Virt manager console" src="./virt-manager2.png" width="45%">
<img alt="Virt manager domain settings" src="./virt-manager3.png" width="45%">
</p>

## Benefits and drawbacks of virtualization

The major benefit we gain from virtualization is the possibility to create virtual hardware X when you have access to physical hardware Y (replace X and Y with operating systems, CPUs, disks, network interfaces). This can cut down on physical installation costs and help with [scalability](https://en.wikipedia.org/wiki/Scalability) of the services.

However the same problems of maintaining multiple separate operating systems/environment still exists. It might even require more resources to manage and update 100 small virtual servers rather than 10 big physical servers. The standard industry practice nowadays is to run one useful service (such as a web server) per machine to maximize isolation and make the systems easier to maintain. The ratio of useful computation has significantly dropped from the mainframe days of computing.

> Maintenance costs always outweigh the initial installation cost in the long run.

One solution that solves some of the issues, like scalable management and wastefulness, caused by virtual machines is containerization and [cloud native computing](https://en.wikipedia.org/wiki/Cloud-native_computing).

## Containerization

[Containerization](https://en.wikipedia.org/wiki/OS-level_virtualization) uses a container runtime to create and manage lightweight execution environments known as containers on a single physical server. Each container shares the host system's _kernel_, but has its own isolated "operating system", file system and network stack. Containers are more lightweight and can be spun up much faster than VMs.

Containerization is often called operating system level virtualization, because containerization is implemented in software with the help of the operating system.

Containers can run different package managers for example, or different versions of same libraries. Their purpose is to be consistent according to the [container image](https://opensource.com/article/21/8/container-image) and to avoid the "works on my machine" situation. Container images are usually built with containerfiles (also known as dockerfiles) which specify a (hopefully) [reproducible](https://reproducible-builds.org/docs/definition/) list of steps.

Containers can be used to run giant and scalable workloads in the cloud using a tool called [Kubernetes](https://kubernetes.io/). Kubernetes is the most used cloud native container [orchestrator](https://en.wikipedia.org/wiki/Orchestration_(computing)) today.

## Podman and Docker

Podman and Docker are containerization platforms used to build, deploy and run containers.

Docker uses a [client-server model](https://en.wikipedia.org/wiki/Client%E2%80%93server_model) where a client, like the `docker` command line tool, connects to the server (`dockerd`, the Docker daemon which connects to [`containerd`](https://earthly.dev/blog/containerd-vs-docker/), a container management server (are you [confused yet?](https://stackoverflow.com/questions/46649592/dockerd-vs-docker-containerd-vs-docker-runc-vs-docker-containerd-ctr-vs-docker-c))) to manage the containers running on that system. Getting access to the docker server is equivalent to getting root access on the system.

Podman, or POD manager, on the otherhand is vastly simpler and does not require a server to function, but instead can use [systemd](https://systemd.io/) to manage the lifecycle of containers. Podman also has support for most operations to be done with user privileges, which is in line with the unix philosophy of least privilege.

## Further reading

Here are some resources to learn more about virtual machines and containers:

- <https://ubuntu.com/server/docs/virtualization-qemu>
- <https://katacontainers.io/learn/>
- TODO

## Feedback

[Open a Gitlab issue](https://gitlab.com/otafablab/self-hosted-lukiokurssi/-/issues)
