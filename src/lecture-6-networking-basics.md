# Lecture 6: Networking Basics

## Learning objectives

- Understands basics of networking.
- Knows the TCP/IP model.
- Knows the basics of Internet routing.
- Knows how to read CIDR notation and its implications.
- Knows what Internet security is and knows when scanning a network is allowed.
- Knows that there exist an uncountable number of IT initialisms such as A, AAA, AAAA, AES, TTL, RTT, MTU, BAUD, USB, LAN, WAN, PXE, ISP, MAC, NAT, CIDR, CPU, API, OS, NIC, OFDM, DDoS, PGP, OSI, QoS, PHP, DB, DMZ, BYOD, SSID, DLS, VPN, WLAN, VLAN, FQDN, FTP, CDN, GSM, HTML, HDMI, HVAC, IANA, ICANN, IETF, RFC, IEEE, GIF, PNG, WEBP, JPEG, SCSI, IPSEC, IT, ATK, KVM, LDAP, LED, FET, LTE, 5G, MBps, MiB, MIMO, PWR, NAS, NFC, NFT, MIPS, NIST, ISO, HDD, FDD, SSD, SSH, RADIUS, PoE, POD, OSI, P2P, PaaS, PC, OSX, GNU, PKI, PSK, RDP, RFID, RIP, TLA.

## Networking

Networking is the practice of connecting computer devices and sending data between them. Here are some good questions to keep in mind while learning networking:

- How does a networked device send and receive data on the network?
- How does the network propagate packets so that devices can communicate across the world?
- Why does the Internet have routers rather than consisting of only switches forming a giant network segment?
- What would happen if you replaced your router with only a switch?
- What happens to the MAC and IP addresses of a packet while it traverses the Internet?

Answers to these questions can be found by practicing using networking hardware and software or by reading this document.

### Resources

Most resources online explain networking and routing poorly and are focused for less technical people. Here are some above average resources.

- [Based explanation of the OSI model](https://www.youtube.com/watch?v=3b_TAYtzuho)
- [Computerphile episode on routing](https://www.youtube.com/watch?v=AkxqkoxErRk)
- [Internet architecture](https://knilt.arcc.albany.edu/images/e/ef/Unit2_LectureNotes_1.pdf)

### Terminology

Here is some key terminology used in networking as well as symbols we will use in diagrams. You can find detailed terminology at [the end of this lecture](#extra-terminology).

| Term                                                                                 | Meaning                                                                                                                                                                                                                                                                                                                                                                                                                                                | Symbol                                               |
|--------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:----------------------------------------------------:|
| [Computer network](https://en.wikipedia.org/wiki/Computer_network)                   | A graph of interconnected devices. Can communicate with each other using various protocols over various media. Other networks include social, neural, electrical and transportation networks.                                                                                                                                                                                                                                                          | ![](./networking/network-network_symbol.drawio.svg)  |
| [Link](https://en.wikipedia.org/wiki/Telecommunications_link)                        | A physical and logical connection between two network devices. Could be a connection between your computer's Ethernet port and a port on a switch.                                                                                                                                                                                                                                                                                                     | ![](./networking/network-link.drawio.svg)            |
| [Network segment](https://en.wikipedia.org/wiki/Broadcast_domain)                    | A network of devices connected together by links and switches. Each network interface device is uniquely identified by its MAC address. Network segments are bounded by routers and [air gaps](https://en.wikipedia.org/wiki/Air_gap_(networking)). A network segment can contain multiple subnets and routers.                                                                                                                                        | ![](./networking/network-network_l2.drawio.svg)      |
| [Subnetwork (subnet)](https://en.wikipedia.org/wiki/Subnetwork)                      | A logical partition of the IP address space. Some subnets can span multiple network segments in larger enterprise networks, where there is a [hierarchy of routers](https://en.wikipedia.org/wiki/Hierarchical_routing).                                                                                                                                                                                                                               | ![](./networking/network-network_l3.drawio.svg)      |
| [Host](https://en.wikipedia.org/wiki/Host_%28network%29)                             | A system with an IP address. May have multiple [NIC](https://en.wikipedia.org/wiki/Network_interface_controller)s and may be connected to multiple networks.                                                                                                                                                                                                                                                                                           | ![](./networking/network-host_symbols.drawio.svg)    |
| [Router](https://en.wikipedia.org/wiki/Router_(computing))                           | A device which connects multiple network segments together and routes IP packets between them. Usually has multiple NICs, but can also use [Virtual LAN](https://en.wikipedia.org/wiki/VLAN)s. Can perform [network address translation](https://en.wikipedia.org/wiki/Network_address_translation) to create a [private subnet](https://en.wikipedia.org/wiki/Private_network) and provide [Internet](https://en.wikipedia.org/wiki/Internet) access. | ![](./networking/network-router_symbol.drawio.svg)   |
| [Switch](https://en.wikipedia.org/wiki/Network_switch)                               | A device which merges network segments with [application specific hardware](https://en.wikipedia.org/wiki/Application-specific_integrated_circuit). A standard switch doesn't have an IP address or even a MAC address, but [multilayer switches](https://en.wikipedia.org/wiki/Multilayer_switch) can communicate on the [Internet layer](https://en.wikipedia.org/wiki/Internet_layer) for web-based configuration for example.                      | ![](./networking/network-switch_symbol.drawio.svg)   |
| [Home router](https://en.wikipedia.org/wiki/Residential_gateway)                     | A router, switch and [WiFi AP](https://en.wikipedia.org/wiki/Wireless_access_point) usually integrated together. Routes IP traffic between the [WAN](https://en.wikipedia.org/wiki/Wide_area_network) (public) and the [LAN](https://en.wikipedia.org/wiki/Local_area_network) (private) network interfaces of the router. Typically installed in residential apartments. Runs a lightweight operating system and a basic firewall.                    | ![](./networking/network-home_router.drawio.svg)     |
| [Firewall](https://en.wikipedia.org/wiki/Firewall_(computing))                       | A software system (often integrated into the kernel) that filters traffic. Firewalls usually block new connections to most TCP and UDP ports.                                                                                                                                                                                                                                                                                                          | ![](./networking/network-firewall_symbol.drawio.svg) |
| [MAC address](https://en.wikipedia.org/wiki/MAC_address)                             |                                                                                                                                                                                                                                                                                                                                                                                                                                                        | ![](./networking/network-mac_addr.drawio.svg)        |
| [IPv4 address](https://en.wikipedia.org/wiki/Internet_Protocol_version_4#Addressing) |                                                                                                                                                                                                                                                                                                                                                                                                                                                        | ![](./networking/network-ip_addr.drawio.svg)         |

### TCP/IP model

All information in this lecture is based on the popular [TCP/IP model](https://en.wikipedia.org/wiki/Internet_protocol_suite). It is a framework for organizing the protocols used on the Internet. It contains tons of protocols on various layers.

> Another popular model which describes internetworking more abstractly is the [OSI model](https://en.wikipedia.org/wiki/OSI_model).

The model has 4 layers with very different semantics:

1. [The Link layer](https://en.wikipedia.org/wiki/Link_layer), where packets are [framed](https://en.wikipedia.org/wiki/Ethernet_frame) and devices use [MAC addresses](https://en.wikipedia.org/wiki/MAC_address) to provide _unique_ identity.
1. [The Internet layer](https://en.wikipedia.org/wiki/Internet_layer), where packets are routed across network boundaries and hosts use [IP addresses](https://en.wikipedia.org/wiki/IP_address) to provide identity.
1. [The Transport layer](https://en.wikipedia.org/wiki/Transport_layer), where reliable connections can be made across the Internet for hosts to communicate. Services use [socket](https://en.wikipedia.org/wiki/Network_socket) addresses to provide unique identity.
1. [The Application layer](https://en.wikipedia.org/wiki/Application_layer), where process-to-process communication happens. Application layer data can be in any format. The most common "unique" addressing scheme for applications are [URI](https://en.wikipedia.org/wiki/Uniform_Resource_Identifier)s which may even provide abstraction and [load-balancing](https://en.wikipedia.org/wiki/Load_balancing_(computing)) over the remote host (such as `wikipedia.org`) using [anycast addressing](https://en.wikipedia.org/wiki/Anycast#Applications).

Systems at each layer are not allowed to change the payload which comes from the layer above.

<img src="https://upload.wikimedia.org/wikipedia/commons/c/c4/IP_stack_connections.svg" style="filter:invert(1) hue-rotate(180deg)">

One intuitive idea behind the layers is that the payload gets encapsulated when going down the stack and layers get decapsulated (peeled off) when going up. This process happens at each layer boundary with hardware, firmware or software.

For example a switch, operating at the Link layer, has access to the MAC addresses in the frames, which it can use to redirect the frame to a specific Ethernet port or all ports. The IP packet with the source and destination IP addresses is left untouched. Also the source and destination MAC addresses must remain untouched while in the same network segment.

> On the other hand, the purpose of a router is to change the MAC addresses because the network segment changes during routing.

### Home Internet Connection

Most networks can be described visually using graphs and additional labeling. Only a small neighborhood is presented to concentrate on what is important.

A typical home network with two clients and a home router might look like the following viewed at the Link layer of the TCP/IP model.

> Notes about these network diagrams:
> - The MAC addresses have been shortened to first and last octet for brevity.
> - The links between the hosts can be Ethernet or WiFi.
> - Any non-electronic connections such as an RJ45 patch panel are not shown as they have no role in the TCP/IP model.

![](./networking/network-home.drawio.svg)

The dashed rectangles represent the two most important networks in a home environment: the local area network (LAN) and the wide area network (WAN).

The LAN usually consists of a single network segment where each device can communicate without traffic ending up in the WAN. In fact, it would be a major security risk if packets in the private LAN subnet leaked to the WAN side (often being the public Internet).

> It is possible that the home router's WAN port is connected to a private network managed by the ISP. This is known as a [Carrier-Grade NAT](https://en.wikipedia.org/wiki/Carrier-grade_NAT). We will assume that this is not the case, and that the router's WAN NIC gets a public IP address.

For `3F:87` to connect to a server on the internet, such as `98:1C` which in this case is the ISP's router, it needs to know the home router's LAN MAC address. It can get that by using [ARP](https://en.wikipedia.org/wiki/Address_Resolution_Protocol) and caching the result in the [ARP cache](https://en.wikipedia.org/wiki/ARP_cache).

> Hosts within a network segment can find each others' MAC addresses by broadcasting an ARP request which asks for the computer with the specified IP address to respond. The ARP request traverses over Ethernet or WiFi mediated by all the switches.

Now that `3F:87` knows the router's address, `10:6F`, it can use it to send IP traffic through it in order to connect to `98:1C`. However, `3F:87` doesn't need to know `98:1C`'s MAC address, but IP address instead, as they are in different network segments.

The logical IP address structure of a home network can be described using the following diagram.

![](./networking/network-home_ip.drawio.svg)

> On the public Internet, routers advertize to their neighboring routers what subnets they can route to (using the infamous [BGP](https://www.cloudflare.com/learning/security/glossary/what-is-bgp/) protocol) and have a mechanism for calculating the shortest/fastest path. A typical home router however does not take part in this and just routes all outbound traffic to the same ISP router.

A couple of notes about the drawing:

- IPv4 addresses have been shortened to first and last octet.
- The IPv4 addresses are in the [CIDR](https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing) notation to describe the relation to their subnet.
- The routers in the public internet do not belong to any subnet (which may or may not be the case depending on the ISP). This is indicated by the `/32` mask.
- The links and switches (and their [topology](https://en.wikipedia.org/wiki/Network_topology)) inside the LAN subnet are implicit and assumed as they do not affect the routing of the packets.
  > Thus, it is sometimes depicted that routing happens between subnets, which is intuitive but misleading. The packets still go through switches and CAT cables and so on.
- There is no WAN subnet. To be specific, it is just the host `94.203/32`.

The way a client, such as `192.55/24`, can send a packet to a remote public server `65.4/24` happens as follows:

TODO how a packet traverses through the network changing mac and ip addresses

## Internet Protocol

> The Internet protocol suite does not presume any specific hardware or software environment.

The [Internet Protocol](https://en.wikipedia.org/wiki/Internet_Protocol) (IP) specifies addressing and how the packets are to be sent and routed through the network. The Internet Protocol assumes very little about the underlying method of data transmission, which can be anything from electric signals to photons in a fiber-optic cable.

The [Internet protocol suite](https://en.wikipedia.org/wiki/Internet_protocol_suite) (TCP/IP model) includes protocols for host management (DHCP), reliable data transmission (TCP), web communication (HTTP), secure shell (SSH), etc...

Internet Protocol version 4, or IPv4 for short, is currently the most widely used networking protocol by devices on the Internet. The other notable protocol is IP version 6. IPv4 addresses are easier to read by humans, but have [run out](https://en.wikipedia.org/wiki/IPv4_address_exhaustion). However IPv6 is superior in many ways so it is the better option in general.

### IPv4 Classless Inter-Domain Routing notation

### Routing

When a host receives a packet it gets decapsulated and the frame header and trailer get stripped off. A router's purpose is to re-encapsulate the IP data in another frame and based on the routing table stored in the router, send that new frame to the appropriate NIC.

### Network Address Translation

### Internet Control Message Protocol and Tracing

TODO how does the packet change through routers

- <https://networkengineering.stackexchange.com/questions/56750/is-it-possible-to-perform-a-layer-2-mac-address-traceroute>

## Transmission Control Protocol

### Connection model

TODO symmetry, flags, sessions

### Ports

TODO socket address

### Common applications

### Port scanning

TODO legal advice

## Linux networking

TODO hosts can have multiple IP addresses in different subnets, or even in the same subnet

TODO default ports

TODO localhost and 0.0.0.0 https://en.wikipedia.org/wiki/0.0.0.0

TODO quick summary of tools and programs: ncat, nmap, ip, mtr, dig, wireshark

### Security

TODO firewalls, NATs, segregation, privacy, MITM, encryption, physical media security considerations.

## Extra terminology

| Term                                                                                          | Meaning                                                                                                                                                                                                                                                                      |
|-----------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Subnet mask                                                                                   | A logical way to separate subnets. Used to provide routing information. A `/32` subnet means that the device isn't connected to a LAN, but it may still communicate according to its routing table.                                                                          |
| Internet                                                                                      | A network of all interconnected devices across all L3 networks. Not all IP addresses need to be unique (private ones in particular). Often misleadingly called "network of networks" which implies a higher structure of connections (networks don't communicate, hosts do). |
| [Open Systems Interconnection model, OSI](https://en.wikipedia.org/wiki/OSI_model)            | A 7-layer abstract description of networking which can be applied to any type of network communication. Describes a more general theoretic model than TCP/IP.                                                                                                                |
| [Network address translation, NAT](https://en.wikipedia.org/wiki/Network_address_translation) | A stateful method of mapping a private subnet (or address) to another across a router. NAT (`map` function) is often confused with a firewall (`filter` function).                                                                                                           |
| Classless Inter-Domain Routing notation, CIDR                                                 |                                                                                                                                                                                                                                                                              |
| Gateway                                                                                       |                                                                                                                                                                                                                                                                              |
| Port                                                                                          | Transport layer and switch ports                                                                                                                                                                                                                                                                             |
| Medium access control, MAC                                                                    |                                                                                                                                                                                                                                                                              |
| [Route](https://en.wikipedia.org/wiki/IP_routing)                                             | An entry in the [routing table](https://en.wikipedia.org/wiki/Routing_table) which specifies which subnet (or host) might be reachable through which network interface. A route only considers the next hop to send packets to.                                              |
| [Backbone](https://en.wikipedia.org/wiki/Backbone_network)                                    |                                                                                                                                                                                                                                                                              |
| LAN                                                                                           | TODO private subnets                                                                                                                                                                                                                                                         |
| WAN                                                                                           | TODO public subnets                                                                                                                                                                                                                                                          |
