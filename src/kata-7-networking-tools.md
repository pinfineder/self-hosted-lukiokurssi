# Kata 7: Linux Networking Tools
## Learning objectives

- Knows the basic protocols of the Internet Protocol Suite, such as DHCP, DNS, HTTP, SSH, IPv4, ICMP, TCP, UDP.
- Can use networking tools to
  - list the NICs on a Linux computer and their addresses
  - list the routes configured on a Linux machine
  - scan a subnet to get information about the hosts therein
  - query DNS names
  - send HTTP(S) requests
  - create an ad-hoc TCP server or connection with netcat
  - configure a firewall on Debian-based hosts
  - trace the route taken by packets across routers

## System target summary

TODO the commands should be handed out, minimal effort

- Use ping to check that some host is pingable
- Use mtr to figure out the ISP's IP address
- Use curl to send a packet somewhere
- Use ufw to setup a firewall which only allows ssh access
- Make a script which reports the output of the following commands
  - TODO ip
  - TODO nmap
  - TODO tracepath
  
TODO use scp to copy the script

TODO advanced socat

## Debriefing

### Local host IP and MAC address tools

We can get information about each NIC along with their associated IP and MAC addresses using the [`ip`](./commands.md#ip) command which comes with most distributions by default. The subcommand `ip address` or `ip a` for short shows a detailed listing of network interfaces. It might take some time to get used to spotting the relevant information in the output.

<pre>
<code class="language-console hljs">$ ip a
1: lo: &lt;LOOPBACK,UP,LOWER_UP&gt; mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback <u>00:00:00:00:00:00</u> brd 00:00:00:00:00:00
    inet <u>127.0.0.1/8</u> scope host lo
       valid_lft forever preferred_lft forever
    inet6 <u>::1/128</u> scope host
       valid_lft forever preferred_lft forever
2: ens3: &lt;BROADCAST,MULTICAST,UP,LOWER_UP&gt; mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether <u>52:54:00:3e:e7:80</u> brd ff:ff:ff:ff:ff:ff
    altname enp0s3
    inet <u>10.42.0.173/24</u> metric 100 brd 10.42.0.255 scope global dynamic ens3
       valid_lft 560sec preferred_lft 560sec
    inet6 <u>fe80::5054:ff:fe3e:e780/64</u> scope link
       valid_lft forever preferred_lft forever
3: virbr0: &lt;NO-CARRIER,BROADCAST,MULTICAST,UP&gt; mtu 1500 qdisc noqueue state DOWN group default qlen 1000
    link/ether <u>52:54:00:fc:a0:53</u> brd ff:ff:ff:ff:ff:ff
    inet <u>192.168.122.1/24</u> brd 192.168.122.255 scope global virbr0
       valid_lft forever preferred_lft forever
9: virbr1: &lt;BROADCAST,MULTICAST,UP,LOWER_UP&gt; mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether <u>52:54:00:40:4c:55</u> brd ff:ff:ff:ff:ff:ff
    inet <u>192.168.121.1/24</u> brd 192.168.121.255 scope global virbr1
       valid_lft forever preferred_lft forever
10: vnet2: &lt;BROADCAST,MULTICAST,UP,LOWER_UP&gt; mtu 1500 qdisc noqueue master virbr1 state UNKNOWN group default qlen 1000
    link/ether <u>da:d2:47:72:53:1a</u> brd ff:ff:ff:ff:ff:ff
    inet6 <u>fe80::fc54:ff:fee4:674f/64</u> scope link
       valid_lft forever preferred_lft forever
</code>
</pre>

> The MAC and IP addresses in the output are underlined.

Information about the routing table can be acquired through the `ip route` subcommand or `ip r` for short.

```console
default via 10.42.0.1 dev ens3 proto dhcp src 10.42.0.173 metric 100
8.8.8.8 via 10.42.0.1 dev ens3 proto dhcp src 10.42.0.173 metric 100
10.42.0.0/24 dev ens3 proto kernel scope link src 10.42.0.173 metric 100
10.42.0.1 dev ens3 proto dhcp scope link src 10.42.0.173 metric 100
192.168.121.0/24 dev virbr1 proto kernel scope link src 192.168.121.1
192.168.122.0/24 dev virbr0 proto kernel scope link src 192.168.122.1 linkdown
```

In this output we can see that the default gateway `10.42.0.1` is in the network segment connected to `ens3`.

If we removed the default gateway using `ip route del default`, we could still contact Google's DNS server `8.8.8.8`, as it has an explicit route.

```console
$ sudo ip r del default
$ ip r
8.8.8.8 via 10.42.0.1 dev ens3 proto dhcp src 10.42.0.173 metric 100
10.42.0.0/24 dev ens3 proto kernel scope link src 10.42.0.173 metric 100
10.42.0.1 dev ens3 proto dhcp scope link src 10.42.0.173 metric 100
192.168.121.0/24 dev virbr1 proto kernel scope link src 192.168.121.1
192.168.122.0/24 dev virbr0 proto kernel scope link src 192.168.122.1 linkdown
$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=56 time=3.76 ms
$ ping 209.51.188.116 # gnu.org
ping: connect: Network is unreachable
```

To add the default route back, we can run [`dhclient`](./commands.md#dhclient) to ask the DHCP server for it.

```console
$ sudo dhclient -v
Listening on LPF/vnet2/da:d2:47:72:53:1a
Sending on   LPF/vnet2/da:d2:47:72:53:1a
Listening on LPF/virbr1/52:54:00:40:4c:55
Sending on   LPF/virbr1/52:54:00:40:4c:55
Listening on LPF/virbr0/52:54:00:fc:a0:53
Sending on   LPF/virbr0/52:54:00:fc:a0:53
Listening on LPF/ens3/52:54:00:3e:e7:80
Sending on   LPF/ens3/52:54:00:3e:e7:80
Sending on   Socket/fallback
DHCPDISCOVER on vnet2 to 255.255.255.255 port 67 interval 3 (xid=0x194f681f)
DHCPDISCOVER on virbr1 to 255.255.255.255 port 67 interval 3 (xid=0xd040e251)
DHCPDISCOVER on virbr0 to 255.255.255.255 port 67 interval 3 (xid=0x1d4e2316)
DHCPREQUEST for 10.42.0.252 on ens3 to 255.255.255.255 port 67 (xid=0x60c74846)
DHCPACK of 10.42.0.252 from 10.42.0.1 (xid=0x4648c760)
RTNETLINK answers: File exists
bound to 10.42.0.252 -- renewal in 234 seconds.
$ ip r
default via 10.42.0.1 dev ens3
8.8.8.8 via 10.42.0.1 dev ens3 proto dhcp src 10.42.0.173 metric 100
10.42.0.0/24 dev ens3 proto kernel scope link src 10.42.0.173 metric 100
10.42.0.1 dev ens3 proto dhcp scope link src 10.42.0.173 metric 100
192.168.121.0/24 dev virbr1 proto kernel scope link src 192.168.121.1
192.168.122.0/24 dev virbr0 proto kernel scope link src 192.168.122.1 linkdown
$ ping 209.51.188.116 # gnu.org
PING 209.51.188.116 (209.51.188.116) 56(84) bytes of data.
64 bytes from 209.51.188.116: icmp_seq=1 ttl=50 time=106 ms
```

### Network/remote system enumeration

Scanning a subnet may be used to find other hosts. The most used tool for this is [`nmap`](./commands.md#nmap), which usually needs to be installed separately through [`apt`](./commands.md#apt) for example.

Remember that scanning networks you do not own or have the right to scan is illegal in most countries.

Here is a good [tutorial](https://www.linux.com/training-tutorials/beginners-guide-nmap/).

Scan the whole fablab internal network with :

```console
$ sudo nmap -sn 10.42.0.0/24
Starting Nmap 7.80 ( https://nmap.org ) at 2023-04-23 12:09 UTC
Nmap scan report for _gateway (10.42.0.1)
Host is up (0.00091s latency).
MAC Address: 90:1B:0E:2E:35:31 (Fujitsu Technology Solutions GmbH)
...
Nmap scan report for 10.42.0.199
Host is up (0.00098s latency).
MAC Address: 52:54:00:36:CB:41 (QEMU virtual NIC)
Nmap scan report for c1-ubuntu-0 (10.42.0.173)
Host is up.
Nmap scan report for c1-ubuntu-0 (10.42.0.252)
Host is up.
Nmap done: 256 IP addresses (68 hosts up) scanned in 1.13 seconds
```

> Root access is required to see the MAC addresses. The server used for the scan `c1-ubuntu-0` had two IP addresses in the fablab internal network because I was playing around with `dhclient`.

### Domain name system

TODO nslookup, dig

### Opening and connecting to sockets

TODO netcat, socat, curl

### Packet capture

TODO wireshark, tshark

### Local firewall configuration

TODO ufw, ss

### ICMP echo and abuse

TODO ping, tracepath, mtr

## Instructions

## Returning and feedback

## Commands and tools
### [`ip`](./commands.md#TOOL)
### [`nmap`](TODO)
### [`nslookup`](TODO)
### [`ping`](TODO)
### [`curl`](TODO)
### [`ncat`](TODO)
### [`socat`](TODO)
### [`ufw`](TODO)
### [`tracepath`](TODO)
### [`mtr`](TODO)
### [`tshark`](TODO)

## Hints

N/A
