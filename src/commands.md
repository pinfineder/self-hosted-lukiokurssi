# List of Linux Commands and Tools

## Table of Contents

<!-- toc -->

## Introduction

This document serves as a reference to commands and tools used in the katas. This document contains the most important Linux shell commands (marked with ⭐) that every hardcore Linux user should know by heart as well as less important commands and tools. The commands are in the order of their appearance.

There are various resources for learning more about a command, the most important of which are:

- Project website, official tutorials (like getting started)
- <https://tldr.sh/>, check [this web-based interactive tldr site](https://tldr.inbrowser.app)
- Manual (man) pages

### How to read this document

For each command, there is an introduction or a short summary, a set of examples **taken directly from** [tldr](https://tldr.sh/), and optionally more references to online materials.

The student is not expected to know how to use each feature of a tool as all options would be nearly impossible to remember. Familiarity with each tool will increase as the tool is used during the course. Feel free to use this page as a problem solving resource.

If you think a tool is missing, send a message or submit an issue.

### Command line commands

TODO explain command line briefly

### Return values

TODO

### Command line shortcuts

What is in common between a wizard and a programmer – Both can cast spells. Such spell casting magic is only made more efficient by the use of _keyboard shortcuts_.

|             Shortcut | description                                                                |
|---------------------:|:---------------------------------------------------------------------------|
|                `Tab` | Autocomplete the command or file name.                                     |
|           `Up arrow` | Show the previous command in the history.                                  |
|         `Down arrow` | Show the next command in the history.                                      |
| `Alt + {Left,Right}` | Move the cursor one word at a time.                                        |
|   `Ctrl + Shift + C` | Copy the selected text.                                                    |
|   `Ctrl + Shift + V` | Paste the copied text.                                                     |
|           `Ctrl + C` | Terminate the current command.                                             |
|           `Ctrl + V` | Doesn't do what you expect. Except in Fish!                                |
|           `Ctrl + R` | Search for a previously used command.                                      |
|           `Ctrl + A` | Move the cursor to the beginning of the line.                              |
|           `Ctrl + E` | Move the cursor to the end of the line.                                    |
|           `Ctrl + W` | Delete the word before the cursor.                                         |
|           `Ctrl + L` | Clear the screen.                                                          |
|           `Ctrl + D` | Log out of the shell. Usually closes the terminal window.                  |
|           `Ctrl + Z` | Suspend the current command, returning to the shell. Use `fg` to get back. |
|           `Ctrl + U` | Clear the line from the cursor position to the beginning of the line.      |
|           `Ctrl + K` | Clear the line from the cursor position to the end of the line.            |
|           `Ctrl + Y` | Restore the deleted text.                                                  |
|            `Alt + .` | Insert the last argument of the previous command.                          |

There are some differences between the different shells. Fish is notably more different from the traditional shells like Bash and Zsh.

In Fish, the shortcuts `Alt + {Up,Down}` are useful for inserting the arguments of previous commands. Also when there is a gray autosuggestion, you can press `Right` or `Ctrl + Right` to apply the entire, or only the next word of the suggestion respectively.

> So the next time you're coding up a storm or waving your wand, remember: you're a wizard with a keyboard, not a keyboard with a wand.
> 
> <p style="display:flex;justify-content:end;">– GhatGPT, 2023</p>

## ⭐`bash`

<!-- ANCHOR:bash -->
Bash (short for Bourne Again SHell) is a widely-used Unix shell for executing commands and scripts in a Linux or Unix-like operating system. A shell is a tool used for running commands.
<!-- ANCHOR_END:bash -->

Bash is the default shell on many Linux distributions and macOS, like CMD.EXE is on Windows, even though better alternatives exist. [sh](https://en.wikipedia.org/wiki/Bourne_shell) is a predecessor of Bash.

> Some other shells [are listed here](https://en.wikipedia.org/wiki/Comparison_of_command_shells).

Bash is often used to automate various tasks such as managing file systems, managing user accounts, monitoring system performance, and managing software and services.

System administrators use shell scripts (written in e.g. Bash) to automate routine and repetitive tasks, making the administration of large, complex systems more manageable and efficient.

The `bash` command just launches a new Bash shell, or if provided with a file path, executes it as a script.

Running commands using a shell is interactive and convenient, but not necessary. The following python script lists the contets of the `/etc` directory **without** using a shell like Bash (of course you need to invoke the python program from somewhere, for example a shell).

```python
from subprocess import check_output

output = check_output(["ls", "-l", "/etc"])
print(output)
```

> The point here is that not everything spawns from a shell, even though it may seem like that when we are interfacing with the operating system using just the shell.

TODO conditionals

### Examples

- Start an interactive shell session:

  ```shell
  bash
  ```

- Execute specific commands:

  ```shell
  bash -c "echo 'bash is executed'"
  ```

- Execute a specific script:

  ```shell
  bash path/to/script.sh
  ```

- Execute a specific script while printing each command before executing it:

  ```shell
  bash -x path/to/script.sh
  ```

- Execute a specific script and stop at the first error:

  ```shell
  bash -e path/to/script.sh
  ```

- Execute specific commands from stdin:

  ```shell
  echo "echo 'bash is executed'" | bash
  ```

- Start an interactive shell session without loading startup configs:

  ```shell
  bash --norc
  ```

## ⭐`cd`

<!-- ANCHOR:cd -->
Changes the current working directory.
<!-- ANCHOR_END:cd -->

### Examples

- Go to the specified directory:

  ```shell
  cd path/to/directory
  ```

- Go up to the parent of the current directory:

  ```shell
  cd ..
  ```

- Go to the home directory of the current user:

  ```shell
  cd
  ```

- Go to the home directory of the specified user:

  ```shell
  cd ~username
  ```

- Go to the previously chosen directory:

  ```shell
  cd -
  ```

## ⭐`ls`

<!-- ANCHOR:ls -->
Lists the contents of a directory.
<!-- ANCHOR_END:ls -->

### Examples

- List files one per line:

  ```shell
  ls -1
  ```
  
- List all files, including hidden files:
  
  ```shell
  ls -a
  ```
  
- List all files, with trailing / added to directory names:
  
  ```shell
  ls -F
  ```
  
- Long format list (permissions, ownership, size, and modification date) of all files:
  
  ```shell
  ls -la
  ```
  
- Long format list with size displayed using human-readable units (KiB, MiB, GiB):
  
  ```shell
  ls -lh
  ```
  
- Long format list sorted by size (descending):
  
  ```shell
  ls -lS
  ```
  
- Long format list of all files, sorted by modification date (oldest first):
  
  ```shell
  ls -ltr
  ```
  
- Only list directories:
  
  ```shell
  ls -d */
  ```

## ⭐`rm`

<!-- ANCHOR:rm -->
Remove files or directories.
<!-- ANCHOR_END:rm -->

### Examples

- Remove specific files:

```shell
rm path/to/file1 path/to/file2 ...
```

- Remove specific files ignoring nonexistent ones:

```shell
rm -f path/to/file1 path/to/file2 ...
```

- Remove specific files interactively prompting before each removal:

```shell
rm -i path/to/file1 path/to/file2 ...
```

- Remove specific files printing info about each removal:

```shell
rm -v path/to/file1 path/to/file2 ...
```

- Remove specific files and directories recursively:

```shell
rm -r path/to/file_or_directory1 path/to/file_or_directory2 ...
```

## ⭐`mkdir`

<!-- ANCHOR:mkdir -->
Creates a new directory.
<!-- ANCHOR_END:mkdir -->

### Examples

- Create specific directories:

  ```shell
  mkdir path/to/directory1 path/to/directory2 ...
  ```

- Create specific directories and their parents if needed:

  ```shell
  mkdir -p path/to/directory1 path/to/directory2 ...
  ```

- Create directories with specific permissions:

  ```shell
  mkdir -m rwxrw-r-- path/to/directory1 path/to/directory2 ...
  ```

## ⭐`echo`

<!-- ANCHOR:echo -->
Writes text to the terminal.
<!-- ANCHOR_END:echo -->

### Examples

- Print a message with environment variables:

  ```shell
  echo "My path is $PATH"
  ```

- Print a message without the trailing newline:

  ```shell
  echo -n "Hello World"
  ```

- Append a message to the file:

  ```shell
  echo "Hello World" >> file.txt
  ```

- Enable interpretation of backslash escapes (special characters):

  ```shell
  echo -e "Column 1\tColumn 2"
  ```

## ⭐`cat`

<!-- ANCHOR:cat -->
Concatenates and displays the contents of files.
<!-- ANCHOR_END:cat -->

### Examples


- Print the contents of a file to the standard output:
  ```shell
  cat path/to/file
  ```

- Concatenate several files into an output file:
  ```shell
  cat path/to/file1 path/to/file2 ... > path/to/output_file
  ```

- Append several files to an output file:
  ```shell
  cat path/to/file1 path/to/file2 ... >> path/to/output_file
  ```

- Copy the contents of a file into an output file without buffering:
  ```shell
  cat -u /dev/tty12 > /dev/tty13
  ```

- Write stdin to a file:
  ```shell
  cat - > path/to/file
  ```

## ⭐`grep`

<!-- ANCHOR:grep -->
Searches for patterns in text.
<!-- ANCHOR_END:grep -->

### Examples

- Search for a pattern within a file:

  ```shell
  grep "search_pattern" path/to/file
  ```

- Search for a pattern in all files recursively in a directory, showing line numbers of matches, ignoring binary files:

  ```shell
  grep --recursive --line-number --binary-files=without-match "search_pattern" path/to/directory
  ```

- Use extended regular expressions (supports ?, +, {}, () and |), in case-insensitive mode:

  ```shell
  grep --extended-regexp --ignore-case "search_pattern" path/to/file
  ```

- Print 3 lines of context around, before, or after each match:

  ```shell
  grep --context|before-context|after-context=3 "search_pattern" path/to/file
  ```
  
  > Shorthands: `-C` = `--context`, `-B` = `--before-context`, `-A` = `--arfter-context`

- Print file name and line number for each match with color output:

  ```shell
  grep --with-filename --line-number --color=always "search_pattern" path/to/file
  ```

- Search for lines matching a pattern, printing only the matched text:

  ```shell
  grep --only-matching "search_pattern" path/to/file
  ```

- Search stdin for lines that do not match a pattern:

  ```shell
  cat path/to/file | grep --invert-match "search_pattern"
  ```

## ⭐`head` and `tail`

<!-- ANCHOR:head_tail -->
Displays the first or last parts of files, respectively.
<!-- ANCHOR_END:head_tail -->

### Examples

- Output the first few lines of a file:

  ```shell
  head -n count path/to/file
  ```

- Show last 'count' lines in file:

  ```shell
  tail --lines count path/to/file
  ```

- Print a file from a specific line number:

  ```shell
  tail --lines +count path/to/file
  ```

- Print a specific count of bytes from the end of a given file:

  ```shell
  tail --bytes count path/to/file
  ```

- Print the last lines of a given file and keep reading file until Ctrl + C:

  ```shell
  tail --follow path/to/file
  ```

- Keep reading file until Ctrl + C, even if the file is inaccessible:

  ```shell
  tail --retry --follow path/to/file
  ```

- Show last 'num' lines in 'file' and refresh every 'n' seconds:

  ```shell
  tail --lines count --sleep-interval seconds --follow path/to/file
  ```
  
## `man`

<!-- ANCHOR:man -->
Format and display manual pages.
<!-- ANCHOR_END:man -->

### Examples

- Display the man page for a command:

```shell
man command
```

- Display the man page for a command from section 7:

```shell
man 7 command
```

- List all available sections for a command:

```shell
man -f command
```

- Display the path searched for manpages:

```shell
man --path
```

- Display the location of a manpage rather than the manpage itself:

```shell
man -w command
```

- Display the man page using a specific locale:

```shell
man command --locale=locale
```

- Search for manpages containing a search string:

```shell
man -k "search_string"
```

## `gcc`

<!-- ANCHOR:gcc -->
The GNU compiler collection. Includes compilers for various programming languages, like `C`, `C++` and Fortran.
<!-- ANCHOR_END:gcc -->

### Examples


- Compile multiple source files into executable:

  ```shell
  gcc path/to/source1.c path/to/source2.c ... -o path/to/output_executable
  ```

- Show common warnings, debug symbols in output, and optimize without affecting debugging:

  ```shell
  gcc path/to/source.c -Wall -g -Og -o path/to/output_executable
  ```

- Include libraries from a different path:

  ```shell
  gcc path/to/source.c -o path/to/output_executable -Ipath/to/header -Lpath/to/library -llibrary_name
  ```

- Compile source code into Assembler instructions:

  ```shell
  gcc -S path/to/source.c
  ```

- Compile source code into an object file without linking:

  ```shell
  gcc -c path/to/source.c
  ```

## ⭐`chmod`

<!-- ANCHOR:chmod -->
Changes the permissions on files and directories.
<!-- ANCHOR_END:chmod -->

### Examples


- Give the user who owns a file the right to execute it:

  ```shell
  chmod u+x path/to/file
  ```

- Give the user rights to read and write to a file/directory:

  ```shell
  chmod u+rw path/to/file_or_directory
  ```

- Remove executable rights from the group:

  ```shell
  chmod g-x path/to/file
  ```

- Give all users rights to read and execute:

  ```shell
  chmod a+rx path/to/file
  ```

- Give others (not in the file owner's group) the same rights as the group:

  ```shell
  chmod o=g path/to/file
  ```

- Remove all rights from others:

  ```shell
  chmod o= path/to/file
  ```

- Change permissions recursively giving group and others the ability to write:

  ```shell
  chmod -R g+w,o+w path/to/directory
  ```
  
- Recursively give all users read permissions to files and e[X]ecute permissions to sub-directories within a directory:

  ```shell
  chmod -R a+rX path/to/directory
  ```

## ⭐`chown`

<!-- ANCHOR:chown -->
Changes the owner of files and directories.
<!-- ANCHOR_END:chown -->

### Examples


- Change the owner user of a file/directory:

  ```shell
  chown user path/to/file_or_directory
  ```

- Change the owner user and group of a file/directory:

  ```shell
  chown user:group path/to/file_or_directory
  ```

- Recursively change the owner of a directory and its contents:

  ```shell
  chown -R user path/to/directory
  ```

- Change the owner of a symbolic link:

  ```shell
  chown -h user path/to/symlink
  ```

- Change the owner of a file/directory to match a reference file:

  ```shell
  chown --reference=path/to/reference_file path/to/file_or_directory
  ```

## `df`

<!-- ANCHOR:df -->
Displays information about the available disk space on file systems.
<!-- ANCHOR_END:df -->

### Examples


- Display all filesystems and their disk usage in human-readable form:

  ```shell
  df -h
  ```

- Display the filesystem and its disk usage containing the given file or directory:

  ```shell
  df path/to/file_or_directory
  ```

- Display statistics on the number of free inodes:

  ```shell
  df -i
  ```

- Display filesystems but exclude the specified types:

  ```shell
  df -x squashfs -x tmpfs
  ```

## `du`

<!-- ANCHOR:du -->
Displays the size of directories and their contents.
<!-- ANCHOR_END:du -->

### Examples


- List the sizes of a directory and any subdirectories, in the given unit (B/KiB/MiB):

  ```shell
  du -b|k|m path/to/directory
  ```

- List the sizes of a directory and any subdirectories, in human-readable form (i.e. auto-selecting the appropriate unit for each size):

  ```shell
  du -h path/to/directory
  ```

- Show the size of a single directory, in human-readable units:

  ```shell
  du -sh path/to/directory
  ```

- List the human-readable sizes of a directory and of all the files and directories within it:

  ```shell
  du -ah path/to/directory
  ```

- List the human-readable sizes of a directory and any subdirectories, up to N levels deep:

  ```shell
  du -h --max-depth=N path/to/directory
  ```

- List the human-readable size of all .jpg files in subdirectories of the current directory (one level deep), and show a cumulative total at the end:

  ```shell
  du -ch */*.jpg
  ```

## ⭐`ps`

<!-- ANCHOR:ps -->
Displays information about the processes running on the system.
<!-- ANCHOR_END:ps -->

### Examples


- List all running processes:

  ```shell
  ps aux
  ```

- List all running processes including the full command string:

  ```shell
  ps auxww
  ```

- Search for a process that matches a string:

  ```shell
  ps aux | grep string
  ```

- List all processes of the current user in extra full format:

  ```shell
  ps --user $(id -u) -F
  ```

- List all processes of the current user as a tree:

  ```shell
  ps --user $(id -u) f
  ```

- Get the parent PID of a process:

  ```shell
  ps -o ppid= -p pid
  ```

- Sort processes by memory consumption:

  ```shell
  ps --sort size
  ```

## ⭐`kill`

<!-- ANCHOR:kill -->
Terminates processes.
<!-- ANCHOR_END:kill -->

Related:
- [`killall`](#killall)
- [`pkill`](#pkill)

### Examples

- Terminate a program using the default SIGTERM (terminate) signal:

  ```shell
  kill process_id
  ```

- List available signal names (to be used without the SIG prefix):

  ```shell
  kill -l
  ```

- Terminate a background job:

  ```shell
  kill %job_id
  ```

- Terminate a program using the SIGHUP (hang up) signal. Many daemons will reload instead of terminating:

  ```shell
  kill -1|HUP process_id
  ```

- Terminate a program using the SIGINT (interrupt) signal. This is typically initiated by the user pressing Ctrl + C:

  ```shell
  kill -2|INT process_id
  ```

- Signal the operating system to immediately terminate a program (which gets no chance to capture the signal):

  ```shell
  kill -9|KILL process_id
  ```

- Signal the operating system to pause a program until a SIGCONT ("continue") signal is received:

  ```shell
  kill -17|STOP process_id
  ```

## ⭐`killall`

<!-- ANCHOR:killall -->
Send kill signal to all instances of a process by name (must be exact name).
<!-- ANCHOR_END:killall -->

Related:
- [`kill`](#kill)
- [`pkill`](#pkill)

### Examples


- Terminate a process using the default SIGTERM (terminate) signal:

  ```shell
  killall process_name
  ```

- list available signal names (to be used without the 'SIG' prefix):

  ```shell
  killall -l
  ```

- Interactively ask for confirmation before termination:

  ```shell
  killall -i process_name
  ```

- Terminate a process using the SIGINT (interrupt) signal, which is the same signal sent by pressing `Ctrl + C`:

  ```shell
  killall -INT process_name
  ```

- Force kill a process:

  ```shell
  killall -KILL process_name
  ```

## ⭐`pkill`

<!-- ANCHOR:pkill -->
Signal processes by name.
<!-- ANCHOR_END:pkill -->

Related:
- [`kill`](#kill)
- [`killall`](#killall)

### Examples

- Kill all processes which match:

  ```shell
  pkill "process_name"
  ```

- Kill all processes which match their full command instead of just the process name:

  ```shell
  pkill -f "command_name"
  ```

- Force kill matching processes (can't be blocked):

  ```shell
  pkill -9 "process_name"
  ```

- Send SIGUSR1 signal to processes which match:

  ```shell
  pkill -USR1 "process_name"
  ```

- Kill the main firefox process to close the browser:

  ```shell
  pkill --oldest "firefox"
  ```

## ⭐`top` and `htop`

<!-- ANCHOR:top_htop -->
Displays a real-time list of processes, sorted by resource usage.
<!-- ANCHOR_END:top_htop -->

### Examples


- Start top:

  ```shell
  top
  ```

- Do not show any idle or zombie processes:

  ```shell
  top -i
  ```

- Show only processes owned by given user:

  ```shell
  top -u username
  ```

- Sort processes by a field:

  ```shell
  top -o field_name
  ```

- Show the individual threads of a given process:

  ```shell
  top -Hp process_id
  ```

- Show only the processes with the given PID(s), passed as a comma-separated list. (Normally you wouldn't know PIDs off hand. This example picks the PIDs from the process name):

  ```shell
  top -p $(pgrep -d ',' process_name)
  ```

## `free`

<!-- ANCHOR:free -->
Display amount of free and used memory in the system.
<!-- ANCHOR_END:free -->

### Examples


- Display system memory:

  ```shell
  free
  ```

- Display memory in Bytes/KB/MB/GB:

  ```shell
  free -b|k|m|g
  ```

- Display memory in human-readable units:

  ```shell
  free -h
  ```

- Refresh the output every 2 seconds:

  ```shell
  free -s 2
  ```

## `ansible`

<!-- ANCHOR:ansible -->
Manage groups of computers remotely over SSH.
<!-- ANCHOR_END:ansible -->

Ansible is an open-source software platform for automating configuration management, deployment, and orchestration of IT infrastructure.

Compared to Bash, Ansible offers a more user-friendly and scalable approach to infrastructure automation, making it a more robust option for managing complex IT infrastructure.

Ansible uses a simple, human-readable language known as **YAML** (Yet Another Markup Language) to describe the desired state of systems, and then uses a series of automated tasks to bring systems into compliance with the defined state.

Ansible is often considered to be more robust than Bash for several reasons:

1. Centralized management: With Ansible, administrators can manage multiple systems from a central location, which makes it much easier to keep track of what's happening across an infrastructure.
1. Idempotence: Ansible ensures that changes made to a system are idempotent, meaning that the same set of commands, when run multiple times, will result in the same end state.
1. Scalability: Ansible is designed to be scalable and can manage large, complex infrastructure composed of hundreds of nodes (running various operating systems) with relative ease.

While Ansible brings a lot of tools to the table, it has multiple drawbacks. For example missing the ability to create composable functions, having non-hygienic namespacing of variables using complex shadowing rules instead, terrible error message often pointing to the wrong line.

### Examples


- List hosts belonging to a group:

  ```shell
  ansible group --list-hosts
  ```

- Ping a group of hosts by invoking the ping module:

  ```shell
  ansible group -m ping
  ```

- Display facts about a group of hosts by invoking the setup module:

  ```shell
  ansible group -m setup
  ```

- Execute a command on a group of hosts by invoking command module with arguments:

  ```shell
  ansible group -m command -a 'my_command'
  ```

- Execute a command with administrative privileges:

  ```shell
  ansible group --become --ask-become-pass -m command -a 'my_command'
  ```

- Execute a command using a custom inventory file:

  ```shell
  ansible group -i inventory_file -m command -a 'my_command'
  ```

- List the groups in an inventory:

  ```shell
  ansible localhost -m debug -a 'var=groups.keys()'
  ```

## ⭐`watch`

<!-- ANCHOR:watch -->
Execute a program periodically, showing output fullscreen.
<!-- ANCHOR_END:watch -->

### Examples


- Repeatedly run a command and show the result:

  ```shell
  watch command
  ```

- Re-run a command every 60 seconds:

  ```shell
  watch -n 60 command
  ```

- Monitor the contents of a directory, highlighting differences as they appear:

  ```shell
  watch -d ls -l
  ```

- Repeatedly run a pipeline and show the result:

  ```shell
  watch 'command_1 | command_2 | command_3'
  ```

## ⭐`ip`

<!-- ANCHOR:ip -->
Show / manipulate routing, devices, policy routing and tunnels.
<!-- ANCHOR_END:ip -->

### Examples

## ⭐`ssh`

<!-- ANCHOR:ssh -->
Securely log onto remote systems. SSH can be used for executing commands on a remote server.
<!-- ANCHOR_END:ssh -->

### Examples

## ⭐`sudo`

<!-- ANCHOR:sudo -->
Executes a single command as the superuser or another user.
<!-- ANCHOR_END:sudo -->

### Examples

## ⭐`apt`

<!-- ANCHOR:apt -->
Package management utility for Debian based distributions.
<!-- ANCHOR_END:apt -->

### Examples

## ⭐`vim`

<!-- ANCHOR:vim -->
Vim (Vi IMproved), a command-line text editor, provides several modes for different kinds of text manipulation. Pressing i in normal mode enters insert mode. Pressing `Esc` goes back to normal mode, which enables the use of Vim commands
<!-- ANCHOR_END:vim -->

### Examples


Open a file:

```shell
vim path/to/file
```

Open a file at a specified line number:

```shell
vim +line_number path/to/file
```

View Vim's help manual:

```shell
:help<Enter>
```

Save and quit the current buffer:

```shell
:wq<Enter>
```

Enter normal mode and undo the last operation:

```shell
<ESC>u
```

Search for a pattern in the file (press n/N to go to next/previous match):

```shell
/search_pattern<Enter>
```

Perform a regular expression substitution in the whole file:

```shell
:%s/regular_expression/replacement/g<Enter>
```

Display the line numbers:

```shell
:set nu<Enter>
```

## `chsh`

<!-- ANCHOR:chsh -->
Change user's login shell.
<!-- ANCHOR_END:chsh -->

### Examples

## `fish`

<!-- ANCHOR:fish -->
The Friendly Interactive SHell, a command-line interpreter designed to be user friendly.
<!-- ANCHOR_END:fish -->

### Examples


Start an interactive shell session:

```shell
fish
```

Start an interactive shell session without loading startup configs:

```shell
fish --no-config
```

Execute specific commands:

```shell
fish --command "echo 'fish is executed'"
```

Execute a specific script:

```shell
fish path/to/script.fish
```

Check a specific script for syntax errors:

```shell
fish --no-execute path/to/script.fish
```

Execute specific commands from stdin:

```shell
echo "echo 'fish is executed'" | fish
```

Start an interactive shell session in private mode, where the shell does not access old history or save new history:

```shell
fish --private
```

Define and export an environmental variable that persists across shell restarts (builtin):

```shell
set --universal --export variable_name variable_value
```

## `neofetch`

> CLI tool to display information about your operating system, software and hardware.
> More information: <https://github.com/dylanaraps/neofetch>.     

### Examples

- Return the default config, and create it if it's the first time 
the program runs:

```neofetch```

- Trigger an info line from appearing in the output, where 'infoname' is the function name in the config file, e.g. memory:

```neofetch --{{enable|disable}} {{infoname}}```

- Hide/Show OS architecture:

```neofetch --os_arch {{on|off}}```

- Enable/Disable CPU brand in output:

```neofetch --cpu_brand {{on|off}}```


## ⭐`su`

<!-- ANCHOR:su -->
Switch shell to another user.
<!-- ANCHOR_END:su -->

### Examples

- Switch to superuser (requires the root password):
  ```shell
  su
  ```

- Switch to a given user (requires the user's password):
  ```shell
  su username
  ```

- Switch to a given user and simulate a full login shell:
  ```shell
  su - username
  ```

- Execute a command as another user:
  ```shell
  su - username -c "command"
  ```

## `vagrant`

> Manage lightweight, reproducible, and portable development environments.
> More information: <https://www.vagrantup.com>.

### Examples

- Create Vagrantfile in current directory with the base Vagrant box:

```vagrant init```

- Create Vagrantfile with the Ubuntu 20.04 (Focal Fossa) box from 
HashiCorp Atlas:

```vagrant init ubuntu/focal64```

- Start and provision the vagrant environment:

```vagrant up```

- Suspend the machine:

```vagrant suspend```

- Halt the machine:

```vagrant halt```

- Connect to machine via SSH:

```vagrant ssh```

- Output the SSH configuration file of the running Vagrant machine:

```vagrant ssh-config```

- List all local boxes:

```vagrant box list```


## `nmap`

> Network exploration tool and security / port scanner.
> Some features only activate when Nmap is run with root privileges.
> More information: <https://nmap.org>.

### Examples

- Check if an IP address is up, and guess the remote host's operating system:

```nmap -O {{ip_or_hostname}}```

- Try to determine whether the specified hosts are up (ping scan) 
and what their names are:

```nmap -sn {{ip_or_hostname}} {{optional_another_address}}```    

- Also enable scripts, service detection, OS fingerprinting and traceroute:

```nmap -A {{address_or_addresses}}```

- Scan a specific list of ports (use '-p-' for all ports from 1 to 65535):

```nmap -p {{port1,port2,...,portN}} {{address_or_addresses}}```  

- Perform service and version detection of the top 1000 ports using default NSE scripts; writing results ('-oN') to output file:    

```nmap -sC -sV -oN {{top-1000-ports.txt}} {{address_or_addresses}}```

- Scan target(s) carefully using 'default and safe' NSE scripts:  

```nmap --script "default and safe" {{address_or_addresses}}```   

- Scan web server running on standard ports 80 and 443 using all available 'http-*' NSE scripts:

```nmap --script "http-*" {{address_or_addresses}} -p 80,443```   

- Perform a stealthy very slow scan ('-T0') trying to avoid detection by IDS/IPS and use decoy ('-D') source IP addresses:

```nmap -T0 -D {{decoy1_ipaddress,decoy2_ipaddress,...,decoyN_ipaddress}} {{address_or_addresses}}```


## ⭐`ping`

> Send ICMP ECHO_REQUEST packets to network hosts.
> More information: <https://manned.org/ping>.

### Examples

- Ping host:

```ping {{host}}```

- Ping a host only a specific number of times:

```ping -c {{count}} {{host}}```

- Ping host, specifying the interval in seconds between requests (default is 1 second):

```ping -i {{seconds}} {{host}}```

- Ping host without trying to lookup symbolic names for addresses:
```ping -n {{host}}```

- Ping host and ring the bell when a packet is received (if your terminal supports it):

```ping -a {{host}}```

- Also display a message if no response was received:

```ping -O {{host}}```

## `dhclient`

> DHCP client.
> More information: <https://manned.org/dhclient>.

### Examples

- Get an IP address for the ```eth0``` interface:

```sudo dhclient {{eth0}}```

- Release an IP address for the ```eth0``` interface:

```sudo dhclient -r {{eth0}}```

## `curl`

> Transfers data from or to a server.
> Supports most protocols, including HTTP, FTP, and POP3.
> More information: <https://curl.se>.

### Examples

- Download the contents of a URL to a file:

```curl {{http://example.com}} --output {{path/to/file}}```       

- Download a file, saving the output under the filename indicated 
by the URL:

```curl --remote-name {{http://example.com/filename}}```

- Download a file, following location redirects, and automatically continuing (resuming) a previous file transfer and return an error on server error:

```curl --fail --remote-name --location --continue-at - {{http://example.com/filename}}```

- Send form-encoded data (POST request of type ```application/x-www-form-urlencoded```). Use ```--data @file_name``` or ```--data @'-'``` to read from STDIN:

```curl --data {{'name=bob'}} {{http://example.com/form}}```      

- Send a request with an extra header, using a custom HTTP method:
```curl --header {{'X-My-Header: 123'}} --request {{PUT}} {{http://example.com}}```

- Send data in JSON format, specifying the appropriate content-type header:

```curl --data {{'{"name":"bob"}'}} --header {{'Content-Type: application/json'}} {{http://example.com/users/1234}}```

- Pass a username and password for server authentication:

```curl --user myusername:mypassword {{http://example.com}}```    

- Pass client certificate and key for a resource, skipping certificate validation:

```curl --cert {{client.pem}} --key {{key.pem}} --insecure {{https://example.com}}```

## `nc`

> Netcat is a versatile utility for working with TCP or UDP data. 
> More information: <https://manned.org/man/nc.1>.

### Examples

- Establish a TCP connection:

```nc {{ip_address}} {{port}}```

- Set a timeout:

```nc -w {{timeout_in_seconds}} {{ipaddress}} {{port}}```

- Scan the open TCP ports of a specified host:

```nc -v -z {{ip_address}} {{port}}```

- Listen on a specified TCP port and print any data received:     

```nc -l {{port}}```

- Keep the server up after the client detaches:

```nc -k -l {{port}}```

- Listen on a specified UDP port and print connection details and 
any data received:

```nc -u -l {{port}}```

- Act as proxy and forward data from a local TCP port to the given remote host:

```nc -l {{local_port}} | nc {{hostname}} {{remote_port}}```      

- Send a HTTP request:

```echo -e "GET / HTTP/1.1\nHost: {{hostname}}\n\n" | nc {{hostname}} 80```

## `ncat`

https://nmap.org/ncat/

> Use the normal ```cat``` functionality over networks.
> More information: <https://manned.org/ncat>.

### Examples

- Listen for input on the specified port and write it to the specified file:

```ncat -l {{port}} > {{path/to/file}}```

- Accept multiple connections and keep ncat open after they have been closed:

```ncat -lk {{port}}```

- Write output of specified file to the specified host on the specified port:

```ncat {{address}} {{port}} < {{path/to/file}}```

## `socat`

> Multipurpose relay (SOcket CAT).
> More information: <http://www.dest-unreach.org/socat/>.

### Examples

- Listen to a port, wait for an incoming connection and transfer data to STDIO:

```socat - TCP-LISTEN:8080,fork```

- Create a connection to a host and port, transfer data in STDIO to connected host:

```socat - TCP4:www.example.com:80```

- Forward incoming data of a local port to another host and port: 

```socat TCP-LISTEN:80,fork TCP4:www.example.com:80```

## `ufw`

> Uncomplicated Firewall.
> Frontend for iptables aiming to make configuration of a firewall easier.
> More information: <https://wiki.ubuntu.com/UncomplicatedFirewall>.

### Examples

- Enable ufw:

```ufw enable```

- Disable ufw:

```ufw disable```

- Show ufw rules, along with their numbers:

```ufw status numbered```

- Allow incoming traffic on port 5432 on this host with a comment 
identifying the service:

```ufw allow {{5432}} comment "{{Service}}"```

- Allow only TCP traffic from 192.168.0.4 to any address on this host, on port 22:

```ufw allow proto {{tcp}} from {{192.168.0.4}} to {{any}} port {{22}}```

- Deny traffic on port 80 on this host:

```ufw deny {{80}}```

- Deny all UDP traffic to ports in range 8412:8500:

```ufw deny proto {{udp}} from {{any}} to {{any}} port {{8412:8500}}```

- Delete a particular rule. The rule number can be retrieved from 
the ```ufw status numbered``` command:

```ufw delete {{rule_number}}```

## `tracepath`

> Trace the path to a network host discovering MTU along this path.
> More information: <https://manned.org/tracepath>.

### Examples

- A preferred way to trace the path to a host:

```tracepath -p {{33434}} {{host}}```

- Specify the initial destination port, useful with non-standard firewall settings:

```tracepath -p {{destination_port}} {{host}} ```

- Print both hostnames and numerical IP addresses:

```tracepath -b {{host}}```

- Specify a maximum TTL (number of hops):

```tracepath -m {{max_hops}} {{host}}```

- Specify the initial packet length (defaults to 65535 for IPv4 and 128000 for IPv6):

```tracepath -l {{packet_length}} {{host}}```

- Use only IPv6 addresses:

```tracepath -6 {{host}}```

## `mtr`

> Matt's Traceroute: combined traceroute and ping tool.
> More information: <https://bitwizard.nl/mtr>.

### Examples

- Traceroute to a host and continuously ping all intermediary hops:

```mtr {{host}}```

- Disable IP address and host name mapping:

```mtr -n {{host}}```

- Generate output after pinging each hop 10 times:

```mtr -w {{host}}```

- Force IP IPv4 or IPV6:

```mtr -4 {{host}}```

- Wait for a given time (in seconds) before sending another packet to the same hop:

```mtr -i {{seconds}} {{host}}```

- Display the Autonomous System Number (ASN) for each hop:        

```mtr --aslookup {{hostname}}```

## `tshark`

> Packet analysis tool, CLI version of Wireshark.
> More information: <https://tshark.dev/>.

### Examples

- Monitor everything on localhost:

```tshark```

- Only capture packets matching a specific capture filter:        

```tshark -f '{{udp port 53}}'```

- Only show packets matching a specific output filter:

```tshark -Y '{{http.request.method == "GET"}}'```

- Decode a TCP port using a specific protocol (e.g. HTTP):        

```tshark -d tcp.port=={{8888}},{{http}}```

- Specify the format of captured output:

```tshark -T {{json|text|ps|â€¦}}```

- Select specific fields to output:

```tshark -T {{fields|ek|json|pdml}} -e {{http.request.method}} -e {{ip.src}}```

- Write captured packet to a file:

```tshark -w {{path/to/file}}```

- Analyze packets from a file:

```tshark -r {{path/to/file.pcap}}```
