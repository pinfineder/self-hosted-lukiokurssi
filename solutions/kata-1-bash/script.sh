#!/bin/sh

# e = fail on error, x = print each command
set -ex

# create directory structure
mkdir --parents src/include
mkdir --parents build

# create src/donut.c
cat <<-'EOF' > src/donut.c
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "donut.h"

int main() {
  float A=0,B=0,i,j,z[1760];
  char b[1760];
  printf("\x1b[2J");
  for(;;) {
    memset(b,32,1760);
    memset(z,0,7040);
    for(j=0;j<6.28;j+=0.07)
    for(i=0;i<6.28;i+=0.02) {
      float c=sin(i),d=cos(j),e=sin(A),f=sin(j),g=cos(A),h=d+2,D=1/(c*h*e+f*g+5),l=cos(i),m=cos(B),n=sin(B),t=c*h*g-f*e;
      int x=40+30*D*(l*h*m-t*n), y=12+15*D*(l*h*n+t*m), o=x+80*y, N=8*((f*e-c*d*g)*m-c*d*e-f*g-l*d*n);
      if(22>y && y>0 && x>0 && x<80 && D>z[o]) {
        z[o]=D;;;b[o]=".,-~:;=!*#$@"[N>0?N:0];
      }
    }
    printf("\x1b[H");
    for(int k=0;k<1761;k++)
    putchar(k%80?b[k]:10);
    A+=INC_A;
    B+=INC_B;
  }
}
EOF

# check if src/include/donut.h does not exists
if [ ! -f "src/include/donut.h" ]; then
    # then create it
    cat <<-'EOF' > src/include/donut.h
#ifndef DONUT_H
#define DONUT_H

float INC_A = 0.004;
float INC_B = 0.002;

#endif
EOF
else
    echo "donut.h exists, not overwriting..."
fi

# compile src/donut.c with all warnings and include src/include. after compiling link with math library and save the binary into build/donut
gcc -Wall -Isrc/include -lm src/donut.c -o build/donut

# execute build/donut
./build/donut
